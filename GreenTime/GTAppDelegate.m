//
//  GTAppDelegate.m
//  GreenTime
//
//  Created by Eduardo Dias on 25/08/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import <Parse/Parse.h>
#import "GTAppDelegate.h"
#import "Client.h"
#import "Project.h"
#import "Timesheet.h"
#import "GTLocationManager.h"
#import "GTRecordTimeViewController.h"

@implementation GTAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
	// Location Service
	[[GTLocationManager instance]updateCurrentLocation];
	
	// Registering Parse
	[Parse setApplicationId:kGTParseAppId clientKey:kGTParseClientKey];
	[self registerParseSubClasses];
	
	// Registering Facebook
	[PFFacebookUtils initializeFacebook];
	
	// Registering Twitter
	[PFTwitterUtils initializeWithConsumerKey:kGTTwitterConsumerKey consumerSecret:kGTTwitterConsumerSecret];
	
	// Customization
	[self customizeApperance];
	
	
	// Register for push notifications
    [application registerForRemoteNotificationTypes:
	 UIRemoteNotificationTypeBadge |
	 UIRemoteNotificationTypeAlert |
	 UIRemoteNotificationTypeSound];
	
	// Setting root view controller
	self.window = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];
	UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
	NSString * rootViewControllerID = ([PFUser currentUser] == nil) ? @"AuthenticationController" : @"TabBarController";
	UIViewController * rootViewController = [storyboard instantiateViewControllerWithIdentifier:rootViewControllerID];
	self.window.rootViewController = rootViewController;
	[self.window makeKeyAndVisible];
	
    return YES;
}

- (void)registerParseSubClasses
{
	[Client registerSubclass];
	[Project registerSubclass];
	[Timesheet registerSubclass];
}

- (void) customizeApperance
{
	// Customizing Tab Bar
	UIImage * tabBarSelectionIndicator = [UIImage imageNamed:@"tabbar_item_bg"];
	[[UITabBar appearance] setSelectionIndicatorImage:tabBarSelectionIndicator];
	
	UIFontDescriptor * fontDescriptor = [UIFontDescriptor fontDescriptorWithFontAttributes:@{UIFontDescriptorFamilyAttribute:kGTFont}];
	
	UIFont * font = [UIFont fontWithDescriptor:fontDescriptor size:0.0];
	
	[UIFont preferredFontForTextStyle:UIFontTextStyleHeadline];
	[[UINavigationBar appearance] setTitleTextAttributes:
	[NSDictionary dictionaryWithObjectsAndKeys:
	 font, NSFontAttributeName, nil]];
	
	[[UINavigationBar appearance] setBarTintColor:[UIColor gtGreen]];
	[[UINavigationBar appearance] setTintColor:[UIColor gtWhite]];
	[[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor gtWhite],
														   NSFontAttributeName:[UIFont fontWithName:kGTFontBold size:17.0]}];
	
	[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
     return [FBAppCall handleOpenURL:url sourceApplication:sourceApplication];
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
	// Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
	// Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
	
	// Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
	// If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
	// Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
	// Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
	// Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)application:(UIApplication *)application
didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)newDeviceToken
{
    // Store the deviceToken in the current installation and save it to Parse.
    PFInstallation * currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation setDeviceTokenFromData:newDeviceToken];
    [currentInstallation saveInBackground];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
	
	[[[UIAlertView alloc] initWithTitle:@"Title" message:@"Push Notification" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil] show];
	
	
   [PFPush handlePush:userInfo];
}

@end
