//
//  NSDate (Utils)
//  GreenTime
//
//  Created by Eduardo Dias on 20/11/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NSDate (Utils)

-(NSString *)preferredDateFormat;
-(NSString *)compactDate;
-(NSString *)timeFormatted;
-(NSDate *)toLocalTime;
-(NSDate *)toGlobalTime;
+(NSString *)timeFromInterval:(CGFloat)interval;
+(NSString *)timeIntervalWithDates:(NSDate *)startDate endDate:(NSDate *)endDate;
+(NSString *)dateRangeWithDates:(NSDate *)startDate endDate:(NSDate *)endDate;

@end
