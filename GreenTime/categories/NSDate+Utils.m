//
//  NSDate (Utils)
//  GreenTime
//
//  Created by Eduardo Dias on 20/11/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

NSString * const kGTCompactDateTemplate = @"dd/MM/yyyy";
NSString * const kGTFullDateTemplate    = @"ccc, dd LLL yyyy";
NSString * const kGTTimeTemplate        = @"hh:mm a";
NSString * const kGTimeIntervalTemplate = @"HH:mm:ss";

@implementation NSDate (Utils)

-(NSString *)preferredDateFormat
{
	NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
	NSString * preferredDateFormat = [userDefaults objectForKey:kGTPreferredDateFormat];
	
	if ([preferredDateFormat isEqualToString:kGTCompactDateFormat])
	{
		return [self compactDate];
	}
	else
	{
		return [self fullDate];
	}
}

- (NSString *)compactDate
{
	NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:kGTCompactDateTemplate];
	[dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
	
	return [dateFormatter stringFromDate:self];
}

- (NSString *)fullDate
{
	NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:kGTFullDateTemplate];
	[dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
	
	return [dateFormatter stringFromDate:self];
}

- (NSString *)timeFormatted
{
	NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:kGTTimeTemplate];
	[dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
	
	return [dateFormatter stringFromDate:self];
}

- (NSDate *)toLocalTime
{
	NSTimeZone * timezone = [NSTimeZone defaultTimeZone];
	NSInteger seconds = [timezone secondsFromGMTForDate: self];
	return [NSDate dateWithTimeInterval: seconds sinceDate: self];
}

- (NSDate *)toGlobalTime
{
	NSTimeZone * timezone = [NSTimeZone defaultTimeZone];
	NSInteger seconds = -[timezone secondsFromGMTForDate: self];
	return [NSDate dateWithTimeInterval: seconds sinceDate: self];
}

+ (NSString *)timeFromInterval:(CGFloat)interval
{
	long time = lroundf(interval);
	
	int hour = time / 3600;
	int mins = (time % 3600) / 60;
	int secs = time % 60;
	
	NSString * hourStr = (hour < 10)?[NSString stringWithFormat:@"%@%d",@"0",hour]:[NSString stringWithFormat:@"%d",hour];
	NSString * minsStr = (mins < 10)?[NSString stringWithFormat:@"%@%d",@"0",mins]:[NSString stringWithFormat:@"%d",mins];
	NSString * secsStr = (secs < 10)?[NSString stringWithFormat:@"%@%d",@"0",secs]:[NSString stringWithFormat:@"%d",secs];
	
	NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
	NSString * displaySeconds = [userDefaults objectForKey:kGTTimesheetReportDisplaySeconds];
	
	if (!displaySeconds || [displaySeconds boolValue])
	{
		return [NSString stringWithFormat:@"%@:%@:%@", hourStr, minsStr, secsStr];
	}
	else
	{
		return [NSString stringWithFormat:@"%@:%@", hourStr, minsStr];
	}
}

+ (NSString *)timeIntervalWithDates:(NSDate *)startDate endDate:(NSDate *)endDate
{
	NSTimeInterval timeInterval = [endDate timeIntervalSinceDate:startDate];
	NSDate * timerDate = [NSDate dateWithTimeIntervalSince1970:timeInterval];
	
	NSDateFormatter * timerFormatter = [[NSDateFormatter alloc] init];
	[timerFormatter setDateFormat:kGTimeIntervalTemplate];
	[timerFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0.0]];
	
	return [timerFormatter stringFromDate:timerDate];
}

+ (NSString *)dateRangeWithDates:(NSDate *)startDate endDate:(NSDate *)endDate
{
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:kGTCompactDateTemplate];
	
	NSString * startDateFT = [dateFormatter stringFromDate:startDate];
	NSString * endDateFT   = [dateFormatter stringFromDate:endDate];
	
	return [NSString stringWithFormat:@"%@ - %@", startDateFT, endDateFT];
}

@end
