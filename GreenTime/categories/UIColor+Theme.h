//
//  UIColor.h
//  GreenTime
//
//  Created by Eduardo Dias on 31/07/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIColor (GreenTimeColors)

+(UIColor *) gtBlack;
+(UIColor *) gtWhite;
+(UIColor *) gtUltraLightGrey;
+(UIColor *) gtLightGrey;
+(UIColor *) gtGrey;
+(UIColor *) gtDarkGrey;
+(UIColor *) gtDarkGreen;
+(UIColor *) gtGreen;
+(UIColor *) gtLightGreen;
+(UIColor *) gtRed;

@end
