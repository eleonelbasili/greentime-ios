//
//  UIColor.m
//  GreenTime
//
//  Created by Eduardo Dias on 31/07/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

@implementation UIColor(GreenTimeColors)

+(UIColor *)gtBlack
{
	return [UIColor colorWithRed:0.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:1];
}

+(UIColor *)gtWhite
{
	return [UIColor colorWithRed:255.0f/255.0f green:255.0f/255.0f blue:255.0f/255.0f alpha:1];
}

+(UIColor *)gtUltraLightGrey
{
	return [UIColor colorWithRed:241.0f/255.0f green:241.0f/255.0f blue:241.0f/255.0f alpha:1];
}

+(UIColor *)gtLightGrey
{
	return [UIColor colorWithRed:239.0f/255.0f green:239.0f/255.0f blue:239.0f/255.0f alpha:1];
}

+(UIColor *)gtGrey
{
	return [UIColor colorWithRed:204.0f/255.0f green:204.0f/255.0f blue:204.0f/255.0f alpha:1];
}

+(UIColor *)gtDarkGrey
{
	return [UIColor colorWithRed:48.0f/255.0f green:48.0f/255.0f blue:48.0f/255.0f alpha:1];
}

+(UIColor *)gtDarkGreen
{
	return [UIColor colorWithRed:66.0f/255.0f green:91.0f/255.0f blue:31.0f/255.0f alpha:1];
}

+(UIColor *)gtGreen
{
	return [UIColor colorWithRed:109.0f/255.0f green:153.0f/255.0f blue:49.0f/255.0f alpha:1];
}

+(UIColor *)gtLightGreen
{
	return [UIColor colorWithRed:157.0f/255.0f green:220.0f/255.0f blue:72.0f/255.0f alpha:1];
}

+(UIColor *)gtRed
{
	return [UIColor colorWithRed:209.0f/255.0f green:31.0f/255.0f blue:40.0f/255.0f alpha:1];
}

@end
