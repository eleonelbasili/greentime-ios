//
//  GTImage.h
//  GreenTime
//
//  Created by Eduardo Dias on 12/09/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIImage (Util)

- (UIImage *)scaleToFit:(CGSize)size;

@end
