//
//  GTImage.m
//  GreenTime
//
//  Created by Eduardo Dias on 12/09/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import "UIImage+ScaleToFit.h"

@implementation UIImage (Util)

- (UIImage *)scaleToFit:(CGSize)size
{
    UIImage *sourceImage = self;
    UIImage *newImage = nil;
	
    CGSize imageSize = sourceImage.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
	
    CGFloat targetWidth = size.width;
    CGFloat targetHeight = size.height;
	
    CGFloat scaleFactor = 0.0;
    CGFloat scaledWidth = targetWidth;
    CGFloat scaledHeight = targetHeight;
	
    CGPoint thumbnailPoint = CGPointMake(0.0,0.0);
	
    if (CGSizeEqualToSize(imageSize, size) == NO) {
		
        CGFloat widthFactor = targetWidth / width;
        CGFloat heightFactor = targetHeight / height;
		
        if (widthFactor < heightFactor)
            scaleFactor = widthFactor;
        else
            scaleFactor = heightFactor;
		
        scaledWidth  = width * scaleFactor;
        scaledHeight = height * scaleFactor;
		
		
        if (widthFactor < heightFactor) {
            thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5;
        } else if (widthFactor > heightFactor) {
            thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
        }
    }
	
    UIGraphicsBeginImageContext(size);
	
    CGRect thumbnailRect = CGRectZero;
    thumbnailRect.origin = thumbnailPoint;
    thumbnailRect.size.width  = scaledWidth;
    thumbnailRect.size.height = scaledHeight;
	
    [sourceImage drawInRect:thumbnailRect];
	
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
	
    if(newImage == nil) NSLog(@"could not scale image");
	
    return newImage ;
}
@end
