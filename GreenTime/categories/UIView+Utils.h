//
//  UIView+Dimensions.h
//  GreenTime
//
//  Created by Eduardo Dias on 23/02/14.
//  Copyright (c) 2014 com.dymextech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Utils)

-(float)height;
-(float)width;
-(float)x;
-(float)y;
-(void)setHeight:(float)h;
-(void)setWidth:(float)w;
-(void)setX:(float)x;
-(void)setY:(float)y;
-(void)increaseHeight:(float)h;
-(void)increaseWidth:(float)w;
-(void)decreaseHeight:(float)h;
-(void)decreaseWidth:(float)w;
-(void)increaseX:(float)x;
-(void)increaseY:(float)y;
-(void)decreaseX:(float)x;
-(void)decreaseY:(float)y;

@end
