//
//  UIView+Dimensions.m
//  GreenTime
//
//  Created by Eduardo Dias on 23/02/14.
//  Copyright (c) 2014 com.dymextech. All rights reserved.
//

#import "UIView+Utils.h"

@implementation UIView (Utils)

-(float)height
{
	return self.frame.size.height;
}

-(float)width
{
	return self.frame.size.width;
}

-(float)x
{
	return self.frame.origin.x;
}

-(float)y
{
	return self.frame.origin.y;
}

-(void)setHeight:(float)h
{
	self.frame = CGRectMake(self.x, self.y, self.width, h);
}

-(void)setWidth:(float)w
{
	self.frame = CGRectMake(self.x, self.y, w, self.height);
}

-(void)setX:(float)x
{
	self.frame = CGRectMake(x, self.y, self.width, self.height);
}

-(void)setY:(float)y
{
	self.frame = CGRectMake(self.x, y, self.width, self.height);
}

-(void)increaseHeight:(float)h
{
	self.frame = CGRectMake(self.x, self.y, self.width, self.height + h);
}

-(void)increaseWidth:(float)w
{
	self.frame = CGRectMake(self.x, self.y, self.height, self.width + w);
}

-(void)decreaseHeight:(float)h
{
	self.frame = CGRectMake(self.x, self.y, self.width, self.height - h);
}

-(void)decreaseWidth:(float)w
{
	self.frame = CGRectMake(self.x, self.y, self.frame.size.width - w, self.frame.size.height);
}

-(void)increaseX:(float)x
{
	self.frame = CGRectMake(self.x + x, self.frame.origin.y, self.frame.size.width, self.height);
}

-(void)increaseY:(float)y
{
	self.frame = CGRectMake(self.x, self.y + y, self.frame.size.width, self.frame.size.height);
}

-(void)decreaseX:(float)x
{
	self.frame = CGRectMake(self.x - x, self.y, self.width, self.height);
}

-(void)decreaseY:(float)y
{
	self.frame = CGRectMake(self.x, self.y - y, self.width, self.height);
}

@end
