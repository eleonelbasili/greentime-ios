//
//  GTBaseButton.m
//  GreenTime
//
//  Created by Eduardo Dias on 15/09/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import "GTBarButtomItem.h"

@implementation GTBarButtomItem

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self)
	{
		NSDictionary *apperanceDictionary = @{NSFontAttributeName:
												  [UIFont fontWithName:kGTFont size:kGTFontSizeMedium]};
		[self setTitleTextAttributes:apperanceDictionary forState:UIControlStateNormal];
	}
    return self;
}

@end
