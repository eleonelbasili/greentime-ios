//
//  GTBaseViewController.h
//  GreenTime
//
//  Created by Eduardo Dias on 28/08/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

@interface GTBaseViewController : UIViewController <MBProgressHUDDelegate>

-(void)showHUD:(int)mode label:(NSString *)label;
-(void)hideHUD;

@end
