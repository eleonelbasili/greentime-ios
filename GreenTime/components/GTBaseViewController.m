//
//  GTBaseViewController.m
//  GreenTime
//
//  Created by Eduardo Dias on 28/08/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import "GTBaseViewController.h"

@interface GTBaseViewController ()

@end

@implementation GTBaseViewController
{
	MBProgressHUD * _HUD;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.view.backgroundColor = [UIColor gtUltraLightGrey];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)showHUD:(int)mode label:(NSString *)label;
{
	_HUD = [MBProgressHUD  showHUDAddedTo:self.view animated:YES];
    _HUD.mode = mode;
    _HUD.delegate = self;
    _HUD.labelText = label;
}

-(void)hideHUD
{
	[self hudWasHidden:_HUD];
}

-(void)hudWasHidden:(MBProgressHUD *)hud
{
	[hud removeFromSuperview];
    _HUD = nil;
}

@end
