//
//  GTButton.h
//  GreenTime
//
//  Created by Eduardo Dias on 9/02/14.
//  Copyright (c) 2014 com.dymextech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GTButtonDelegate.h"

extern const double kGTAnimationDuration;
extern const double kGTCircleStartAngle;
extern const double kGTCircleEndAngle;
extern const double kGTMiterLimit;
extern const double kGTLineWidth;

@interface GTButton : UIButton

@property(nonatomic, strong)id <GTButtonDelegate> delegate;

@end
