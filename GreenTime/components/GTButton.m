//
//  GTButton.m
//  GreenTime
//
//  Created by Eduardo Dias on 9/02/14.
//  Copyright (c) 2014 com.dymextech. All rights reserved.
//

#import "GTButton.h"

#define RADIUS self.bounds.size.width / 2

const double kGTAnimationDuration = 0.7;
const double kGTCircleStartAngle = 0.0;
const double kGTCircleEndAngle = M_PI * 2;
const double kGTMiterLimit = -10;
const double kGTLineWidth = 2.5;

@implementation GTButton
{
	CAShapeLayer * _playCircleLayer;
	CAShapeLayer * _stopCircleLayer;
	CAShapeLayer * _fillShapeLayer;
	
	BOOL   _isInStopState;
	BOOL   _isAnimating;
	double _circleGap;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
	{
		// Initializing properties
		_isInStopState = YES;
		_isAnimating = NO;
		_circleGap = RADIUS / 1.5;
		
		// Adding touch event
		[self addTarget:self action:@selector(toggleState:) forControlEvents:UIControlEventTouchUpInside];
    }
    return self;
}


-(void)toggleState:(id)sender
{
	if (_isAnimating || self.isEnabled != YES)return;
	
	_isAnimating = true;
	
	if (_isInStopState)
	{
		// Square path
		UIBezierPath * squarePath = [self squarePath];
		
		// Red circle animation
		CABasicAnimation * redCircleAnimation  = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
		redCircleAnimation.duration            = kGTAnimationDuration;
		redCircleAnimation.repeatCount         = 0.0;
		redCircleAnimation.removedOnCompletion = NO;
		redCircleAnimation.fillMode            = kCAFillModeForwards;
		redCircleAnimation.fromValue           = [NSNumber numberWithFloat:0.0];
		redCircleAnimation.toValue             = [NSNumber numberWithFloat:1.0];
		redCircleAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
		
		// Square animation
		CABasicAnimation * triangleAnimation = [CABasicAnimation animationWithKeyPath:@"path"];
		triangleAnimation.duration = kGTAnimationDuration;
		triangleAnimation.fromValue = (id)_fillShapeLayer.path;
		triangleAnimation.toValue = (id)squarePath.CGPath;
		triangleAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
		
		// Fill layer
		_fillShapeLayer.path = squarePath.CGPath;
		_fillShapeLayer.fillColor = [[UIColor gtGreen] CGColor];
		[_fillShapeLayer addAnimation:triangleAnimation forKey:@"path"];
		
		// Circle paths
		_stopCircleLayer.strokeColor = [UIColor gtRed].CGColor;
		_playCircleLayer.strokeColor = [UIColor gtGreen].CGColor;
		_stopCircleLayer.path = [self circlePath:kGTCircleStartAngle endAngle:kGTCircleEndAngle];
		[CATransaction begin];
		{
			[CATransaction setCompletionBlock:^{
				_fillShapeLayer.fillColor = [[UIColor gtRed] CGColor];
				_isInStopState = NO;
				_isAnimating = NO;
				if (self.delegate && [self.delegate respondsToSelector:@selector(buttonDidEnterOnPlayState)])
				{
					[self.delegate performSelector:@selector(buttonDidEnterOnPlayState)];
				}
			}];
			[_stopCircleLayer addAnimation:redCircleAnimation forKey:@"redCircleAnimation"];
		}[CATransaction commit];
	}
	else
	{
		// Triangle path
		UIBezierPath * trianglePath = [self trianglePath];
		
		// Square animation
		CABasicAnimation * squareAnimation = [CABasicAnimation animationWithKeyPath:@"path"];
		squareAnimation.duration = kGTAnimationDuration;
		squareAnimation.fromValue = (id)_fillShapeLayer.path;
		squareAnimation.toValue = (id)trianglePath.CGPath;
		squareAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
		
		// Hide red circle animation
		CABasicAnimation * redCircleAnimation  = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
		redCircleAnimation.duration            = kGTAnimationDuration;
		redCircleAnimation.repeatCount         = 0.0;
		redCircleAnimation.removedOnCompletion = NO;
		redCircleAnimation.fillMode            = kCAFillModeForwards;
		redCircleAnimation.fromValue           = [NSNumber numberWithFloat:1.0];
		redCircleAnimation.toValue             = [NSNumber numberWithFloat:0.0];
		redCircleAnimation.timingFunction      = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
		
		// Fill layer
		_fillShapeLayer.path = [self trianglePath].CGPath;
		_fillShapeLayer.fillColor = [[UIColor gtRed] CGColor];
		[_fillShapeLayer addAnimation:squareAnimation forKey:@"path"];
		
		// Circle paths
		_stopCircleLayer.strokeColor = [UIColor gtRed].CGColor;
		_playCircleLayer.strokeColor = [UIColor gtGreen].CGColor;
		_stopCircleLayer.path = [self circlePath:kGTCircleEndAngle endAngle:kGTCircleStartAngle];
		
		// Transaction
		[CATransaction begin];
		{
			[CATransaction setCompletionBlock:^
			{
				_fillShapeLayer.fillColor = [[UIColor gtGreen] CGColor];
				_isInStopState = YES;
				_isAnimating = NO;
				if (self.delegate && [self.delegate respondsToSelector:@selector(buttonDidEnterOnStopState)])
				{
					[self.delegate performSelector:@selector(buttonDidEnterOnStopState)];
				}
			}];
			[_stopCircleLayer addAnimation:redCircleAnimation forKey:@"redCircleAnimation"];
		}[CATransaction commit];
	}
}

-(UIBezierPath *)trianglePath
{
	UIBezierPath * trianglePath = [[UIBezierPath alloc] init];
	[trianglePath moveToPoint: CGPointMake(self.bounds.size.width - RADIUS / 3, self.bounds.size.height / 2)];
	[trianglePath addLineToPoint:CGPointMake(self.bounds.size.width - RADIUS / 3, self.bounds.size.height / 2)];
	[trianglePath addLineToPoint:CGPointMake(_circleGap, self.bounds.size.height - RADIUS / 3)];
	[trianglePath addLineToPoint:CGPointMake(_circleGap, RADIUS / 3)];
	[trianglePath closePath];
	
	return trianglePath;
}

-(UIBezierPath *)squarePath
{
	UIBezierPath * squarePath = [[UIBezierPath alloc] init];
	[squarePath moveToPoint: CGPointMake(self.bounds.size.width - _circleGap, _circleGap)];
	[squarePath addLineToPoint:CGPointMake(self.bounds.size.width - _circleGap, self.bounds.size.height - _circleGap)];
	[squarePath addLineToPoint:CGPointMake(_circleGap, self.bounds.size.height - _circleGap)];
	[squarePath addLineToPoint:CGPointMake(_circleGap, _circleGap)];
	[squarePath closePath];
	
	return squarePath;
}

-(CGPathRef)circlePath:(CGFloat)startAngle endAngle:(CGFloat)endAngle
{
	float xCenterPoint = round(self.bounds.size.width / 2) + 0.5f;
	float yCenterPoint = round(self.bounds.size.height / 2) + 0.5f;
	
	// Drawing stroke
	CGPathRef circlePath = [UIBezierPath
							bezierPathWithArcCenter:CGPointMake(xCenterPoint, yCenterPoint)
							radius:RADIUS
							startAngle:startAngle
							endAngle:endAngle
							clockwise:YES].CGPath;
	return circlePath;
}

- (void)drawRect:(CGRect)rect
{
	if (_isAnimating ) return;
	
	if (!_playCircleLayer)
	{
		// The green circle layer
		_playCircleLayer = [[CAShapeLayer alloc] init];
		_playCircleLayer.frame = self.bounds;
		_playCircleLayer.fillColor   = [UIColor gtWhite].CGColor;
		_playCircleLayer.strokeColor = [UIColor gtGreen].CGColor;
		_playCircleLayer.miterLimit  = kGTMiterLimit;
		_playCircleLayer.lineWidth   = kGTLineWidth;
		[self.layer addSublayer: _playCircleLayer];
	}
	
	if (!_stopCircleLayer)
	{
		// The red circle layer
		_stopCircleLayer = [[CAShapeLayer alloc] init];
		_stopCircleLayer.frame = self.bounds;
		_stopCircleLayer.miterLimit  = kGTMiterLimit;
		_stopCircleLayer.lineWidth   = kGTLineWidth;
		_stopCircleLayer.strokeColor = [UIColor gtRed].CGColor;
		_stopCircleLayer.strokeStart = 0;
		_stopCircleLayer.strokeEnd   = 1;
		_stopCircleLayer.fillColor   = [UIColor clearColor].CGColor;
		[self.layer addSublayer: _stopCircleLayer];
	}
	
	if (!_fillShapeLayer)
	{
		// Fill layer
		_fillShapeLayer = [[CAShapeLayer alloc] init];
		_fillShapeLayer.frame = self.bounds;
		[self.layer addSublayer: _fillShapeLayer];
	}
	
	// UIColor
	UIColor * color = (_isInStopState) ? [UIColor gtGreen] : [UIColor gtRed];
	
	// Drawing circle
	_playCircleLayer.path = [self circlePath:kGTCircleStartAngle endAngle:kGTCircleEndAngle];
	_playCircleLayer.strokeColor = (self.enabled) ? [UIColor gtGreen].CGColor : [UIColor gtGrey].CGColor;
	
	// Shape layer
	_fillShapeLayer.path = (_isInStopState) ? [self trianglePath].CGPath : [self squarePath].CGPath;
	_fillShapeLayer.fillColor = (self.enabled) ? color.CGColor : [UIColor gtGrey].CGColor;
}

@end
