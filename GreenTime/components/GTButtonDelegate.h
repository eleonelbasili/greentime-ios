//
//  GTButtonDelegate.h
//  GreenTime
//
//  Created by Eduardo Dias on 11/02/14.
//  Copyright (c) 2014 com.dymextech. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol GTButtonDelegate <NSObject>

-(void)buttonDidEnterOnStopState;
-(void)buttonDidEnterOnPlayState;

@end
