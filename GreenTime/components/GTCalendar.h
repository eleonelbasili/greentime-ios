//
//  GTCalendarUtils.h
//  GreenTime
//
//  Created by Eduardo Dias on 22/11/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DSLCalendarView.h"
#import "GTCalendarDelegate.h"

extern NSString * const kGTCalendarDidShow;
extern NSString * const kGTCalendarDidHide;

@interface GTCalendar : UIView <DSLCalendarViewDelegate>

-(GTCalendar *)initWithDelegate:(id<GTCalendarDelegate>)delegate
					  initFrame:(CGRect)initFrame
					 finalFrame:(CGRect)finalFrame;

@property(nonatomic, weak)id <GTCalendarDelegate> delegate;
@property(nonatomic)BOOL isVisible;

-(void)setButtonTitle:(NSString *)title;

-(void)show;
-(void)hide;
-(void)toggle;

@end
