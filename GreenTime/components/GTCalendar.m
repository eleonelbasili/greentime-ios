//
//  GTCalendarUtils.m
//  GreenTime
//
//  Created by Eduardo Dias on 22/11/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import "GTCalendar.h"
#import "GTCalendarDelegate.h"

@implementation GTCalendar
{
	DSLCalendarView  * _calendarView;
	DSLCalendarRange * _selectedDateRange;
	CGRect _startFrame;
	CGRect _endFrame;
	
	UIButton * _button;
}

-(GTCalendar *)initWithDelegate:(id<GTCalendarDelegate>)delegate
					  initFrame:(CGRect)initFrame
					 finalFrame:(CGRect)finalFrame;
{
	self = [super initWithFrame:initFrame];
	
	if (self)
	{
		self.backgroundColor = [UIColor gtWhite];
		self.delegate = delegate;
		
		_startFrame = initFrame;
		_endFrame = finalFrame;
		
		_calendarView = [[DSLCalendarView alloc] initWithFrame:CGRectMake(0, 0, 320, 305)];
		_calendarView.delegate = self;
		[self addSubview:_calendarView];
				
		_button = [[UIButton alloc] initWithFrame:[self buttonFrame]];
		[_button setTitle:NSLocalizedString(@"search", nil)forState:UIControlStateNormal];
		[_button.layer setCornerRadius:kGTCornerRadius];
		_button.backgroundColor = [UIColor gtGreen];
		[_button addTarget:self
				   action:@selector(seachButtonPressed:)
		 forControlEvents:UIControlEventTouchUpInside];
		
		[self addSubview:_button];
	}
	return self;
}

-(CGRect)buttonFrame
{
	CGFloat buttonRegionHeight = self.frame.size.height - _calendarView.frame.size.height;
	CGFloat buttonMargin = IS_IPHONE_5 ? 10 : 5;
	CGFloat buttonHeight = buttonRegionHeight * 0.8;
	CGFloat buttonWidth  = self.frame.size.width - (buttonMargin * 2);
	CGFloat buttonYpos   = _calendarView.frame.size.height + buttonRegionHeight / 2 - buttonHeight / 2;
	CGRect buttonRect    = CGRectMake(buttonMargin, buttonYpos, buttonWidth, buttonHeight);
	
	return buttonRect;
}

- (void)calendarView:(DSLCalendarView *)calendarView willChangeToVisibleMonth:(NSDateComponents*)month duration:(NSTimeInterval)duration;
{
	[UIView animateWithDuration:duration delay:0 options:UIViewAnimationOptionCurveLinear animations:^
	{
		_button.frame = [self buttonFrame];
	} completion:nil];
}

-(void)setButtonTitle:(NSString *)title
{
	[_button setTitle:title forState:UIControlStateNormal];
}

-(void)seachButtonPressed:(id)sender
{
	if ([self hasDelegate])
	{
		if (_selectedDateRange)
		{
			[self.delegate selectedDates:_selectedDateRange.startDay.date
								 endDate:_selectedDateRange.endDay.date];
		}
		else
		{
			[self.delegate selectedDates:nil endDate:nil];
		}
		
	}
}

-(BOOL)hasDelegate
{
	return (self.delegate && [self.delegate conformsToProtocol:@protocol(GTCalendarDelegate)]);
}

-(void)calendarView:(DSLCalendarView *)calendarView didSelectRange:(DSLCalendarRange *)dateRange
{
	_selectedDateRange = dateRange;
}

-(void)show
{
	if (self.isVisible)return;
	
	[UIView animateWithDuration:0.5
						  delay:0.0
						options:UIViewAnimationOptionCurveEaseOut animations:^
	 {
		 self.frame = _endFrame;
	 } completion:^(BOOL finished)
	 {
		 self.isVisible = YES;
		 if ([self hasDelegate])
		 {
			 [self.delegate performSelector:@selector(calendarDidShow) withObject:nil];
		 }
	 }];
}

-(void)hide
{
	if (!self.isVisible)return;
	
	[UIView animateWithDuration:0.5
						  delay:0.0
						options:UIViewAnimationOptionCurveEaseOut animations:^
	 {
		 self.frame = _startFrame;
	 } completion:^(BOOL finished)
	 {
		 self.isVisible = NO;
		 if ([self hasDelegate])
		 {
			 [self.delegate performSelector:@selector(calendarDidHide) withObject:nil];
		 }
	 }];
}

-(void)toggle
{
	(self.isVisible)? [self hide] : [self show];
}

@end
