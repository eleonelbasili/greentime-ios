//
//  GTCalendarDelegate.h
//  GreenTime
//
//  Created by Eduardo Dias on 23/11/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GTCalendar.h"

@protocol GTCalendarDelegate <NSObject>

-(void)selectedDates:(NSDate *)startDate endDate:(NSDate *)endDate;
-(void)calendarDidShow;
-(void)calendarDidHide;

@end
