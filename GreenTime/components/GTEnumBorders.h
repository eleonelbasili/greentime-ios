//
//  GTTextInputBorders.h
//  GreenTime
//
//  Created by Eduardo Dias on 2/12/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import <Foundation/Foundation.h>

enum borders
{
	BorderTop    = 1 << 0,
	BorderRight  = 1 << 1,
	BorderBottom = 1 << 2,
	BorderLeft   = 1 << 3
};
