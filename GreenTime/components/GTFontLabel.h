//
//  BOFontLabel.h
//  GreenTime
//
//  Created by Eduardo Dias on 30/07/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GTFontLabel : UILabel

-(id)initWithFrame:(CGRect)frame fontSize:(NSInteger)fontSize;
-(id)initWithFrame:(CGRect)frame font:(UIFont *)font;

@end
