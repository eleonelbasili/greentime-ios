//
//  BOFontLabel.m
//  GreenTime
//
//  Created by Eduardo Dias on 30/07/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import "GTFontLabel.h"

@implementation GTFontLabel

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self)
	{
		self.font = [UIFont fontWithName:kGTFont size:self.font.pointSize];
		self.highlightedTextColor = [UIColor gtGreen];
    }
    return self;
}

-(id)init
{
	self = [super init];
	if (self)
	{
		self.font = [UIFont fontWithName:kGTFont size:self.font.pointSize];
	}
	return self;
}

-(id)initWithFrame:(CGRect)frame
{
	self = [super initWithFrame:frame];
	if (self)
	{
		self.font = [UIFont fontWithName:kGTFont size:self.font.pointSize];
	}
	return self;
}

-(id)initWithFrame:(CGRect)frame fontSize:(NSInteger)fontSize;
{
	self = [super initWithFrame:frame];
	if (self)
	{
		self.font = [UIFont fontWithName:kGTFont size:fontSize];
	}
	return self;
}

-(id)initWithFrame:(CGRect)frame font:(UIFont *)font
{
	self = [super initWithFrame:frame];
	if (self)
	{
		self.font = font;
	}
	return self;
}

@end
