//
//  GTFormTableViewController.h
//  GreenTime
//
//  Created by Eduardo Dias on 29/09/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GTToolBarUtil.h"
#import "MBProgressHUD.h"
#import "GTTextField.h"
#import "GTTextView.h"

@interface GTFormTableViewController : UITableViewController <UITextFieldDelegate, UITextViewDelegate, UITableViewDataSource, MBProgressHUDDelegate>

-(void)showHUD:(int)mode label:(NSString *)label;
-(void)hideHUD;
-(void)setupTextFieldNavigation:(id <UITextInput>)textInput
						forward:(id <UITextInput>)forward
					   backward:(id<UITextInput>)backward;

- (void)keyboardWillShow:(NSNotification *)notification;
- (void)keyboardDidShow:(NSNotification *)notification;
- (void)keyboardWillHide:(NSNotification *)notification;

-(GTToolBarUtil *)toolbar;
-(id<UITextInput>)textInputInEdition;

@end
