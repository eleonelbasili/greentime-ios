//
//  GTFormTableViewController.m
//  GreenTime
//
//  Created by Eduardo Dias on 29/09/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import "GTFormTableViewController.h"
#import "GTTextField.h"
#import "GTTextView.h"

@interface GTFormTableViewController ()

@end

@implementation GTFormTableViewController
{
	id<UITextInput> _textInputInEdition;
	GTToolBarUtil * _gtToolBar;
	MBProgressHUD * _HUD;
}

#pragma mark - Initialization

-(id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if (self)
	{
		_gtToolBar = [[GTToolBarUtil alloc]initWithSelectors:self
												 previous:@selector(navigateToPreviousTextInput)
													 next:@selector(navigateToNextTextInput)
													   done:@selector(resignInputView)];
		
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(keyboardWillShow:)
													 name:UIKeyboardWillShowNotification
												   object:nil];
		
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(keyboardDidShow:)
													 name:UIKeyboardDidShowNotification
												   object:nil];
		
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(keyboardWillHide:)
													 name:UIKeyboardWillHideNotification
												   object:nil];
	}
	return self;
}

-(void)viewDidLoad
{
	[super viewDidLoad];
	
	UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 1)];
	view.backgroundColor = [UIColor clearColor];
	[self.tableView setTableFooterView:view];
}


#pragma mark - TextField Operations


-(void)setupTextFieldNavigation:(id <UITextInput>)textInput
						forward:(id <UITextInput>)forward
					   backward:(id<UITextInput>)backward
{
	if([textInput isKindOfClass:[GTTextField class]])
	{
		GTTextField * field = (GTTextField *) textInput;
		field.delegate = self;
		field.nextTextInput = forward;
		field.previousTextInput = backward;
		[field setInputAccessoryView:_gtToolBar];
	}
	else if([textInput isKindOfClass:[GTTextView class]])
	{
		GTTextField * field = (GTTextField *) textInput;
		field.delegate = self;
		field.nextTextInput = forward;
		field.previousTextInput = backward;
		[field setInputAccessoryView:_gtToolBar];
	}
}

-(void)textFieldDidBeginEditing:(GTTextField *)textField
{
	_textInputInEdition = textField;
	_gtToolBar.previousButton.enabled = (textField.previousTextInput != nil);
	_gtToolBar.nextButton.enabled = (textField.nextTextInput != nil);
}

-(void)textViewDidBeginEditing:(GTTextView *)textView
{
	_textInputInEdition = textView;
	_gtToolBar.previousButton.enabled = (textView.previousTextInput != nil);
	_gtToolBar.nextButton.enabled = (textView.nextTextInput != nil);
}

-(void)navigateToPreviousTextInput
{
	GTTextField * textFieldInEdition = (GTTextField *)_textInputInEdition;
	UIResponder * nextResponder = textFieldInEdition.previousTextInput;
	if(nextResponder)
	{
		[nextResponder becomeFirstResponder];
	}
}


#pragma mark - Toolbar Operations

-(GTToolBarUtil *)toolbar
{
	return _gtToolBar;
}

-(id<UITextInput>)textInputInEdition
{
	return _textInputInEdition;
}


#pragma mark - Navigation Operations

-(void)navigateToNextTextInput
{
	GTTextField * textFieldInEdition = (GTTextField *)_textInputInEdition;
	UIResponder * nextResponder = textFieldInEdition.nextTextInput;
	if(nextResponder)
	{
		[nextResponder becomeFirstResponder];
	}
}

-(void)resignInputView
{
	[(UIResponder *)_textInputInEdition resignFirstResponder];
	_textInputInEdition = nil;
}


#pragma mark - Keyboard Operations

- (void)keyboardWillShow:(NSNotification *)notification
{
	// Override in subclass in case want to listen Keyboard notifications
}

- (void)keyboardDidShow:(NSNotification *)notification
{
	// Override in subclass in case want to listen Keyboard notifications
}

- (void)keyboardWillHide:(NSNotification *)notification
{
	_textInputInEdition = nil;
}


#pragma mark - HUD Operations

-(void)showHUD:(int)mode label:(NSString *)label;
{
	_HUD = [MBProgressHUD  showHUDAddedTo:self.view animated:YES];
    _HUD.mode = mode;
    _HUD.delegate = self;
    _HUD.labelText = label;
}

-(void)hideHUD
{
	[self hudWasHidden:_HUD];
}

-(void)hudWasHidden:(MBProgressHUD *)hud
{
	[hud removeFromSuperview];
    _HUD = nil;
}

@end
