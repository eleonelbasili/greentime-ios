//
//  GTTextField.h
//  GreenTime
//
//  Created by Eduardo Dias on 18/09/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GTEnumBorders.h"

@interface GTTextField : UITextField

@property(nonatomic, weak) id <UITextInput> nextTextInput;
@property(nonatomic, weak) id <UITextInput> previousTextInput;
@property (nonatomic, assign) UIEdgeInsets edgeInsets;
@property (nonatomic) BOOL highlighted;
@property(nonatomic)int borders;

-(void)setContentInsets:(UIEdgeInsets)edgeInsets;

@end
