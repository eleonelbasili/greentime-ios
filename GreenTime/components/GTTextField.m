//
//  GTTextField.m
//  GreenTime
//
//  Created by Eduardo Dias on 18/09/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import "GTTextField.h"

typedef NSUInteger borders;

@implementation GTTextField

@synthesize highlighted = _highlighted;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
	{
		self.font = [UIFont fontWithName:kGTFont size:kGTFontSizeMedium];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self)
	{
		self.font = [UIFont fontWithName:kGTFont size:kGTFontSizeMedium];
    }
    return self;
}

-(void)setEnabled:(BOOL)enabled
{
	[super setEnabled:enabled];
	self.textColor = (enabled) ? [UIColor gtBlack] : [UIColor gtGrey];
}

-(void)setHighlighted:(BOOL)highlighted
{
	if (_highlighted != highlighted)
	{
		_highlighted = highlighted;
		self.textColor = (_highlighted) ? [UIColor gtBlack] : [UIColor gtGreen];
	}
}

-(void)drawRect:(CGRect)rect
{
	[super drawRect:rect];
	
	CGContextRef context = UIGraphicsGetCurrentContext();
	CGContextSetShouldAntialias(context, NO);
	CGContextSetStrokeColorWithColor(context, [UIColor gtGrey].CGColor);
	CGContextSetLineWidth(context, 0.5f);
	
	if (_borders & BorderBottom)
	{
		CGContextMoveToPoint(context, 0.0f, rect.size.height);
		CGContextAddLineToPoint(context, rect.size.width, rect.size.height);
	}
	if (_borders & BorderRight)
	{
		CGContextMoveToPoint(context, rect.size.width - 0.5f, 0);
		CGContextAddLineToPoint(context, rect.size.width - 0.5f, rect.size.height);
	}
	
	CGContextStrokePath(context);
}


-(CGRect)textRectForBounds:(CGRect)bounds
{
	return [super textRectForBounds:UIEdgeInsetsInsetRect(bounds, self.edgeInsets)];
}

- (CGRect)editingRectForBounds:(CGRect)bounds
{
    return [super editingRectForBounds:UIEdgeInsetsInsetRect(bounds, self.edgeInsets)];
}

-(void)setContentInsets:(UIEdgeInsets)edgeInsets;
{
	self.edgeInsets = edgeInsets;
	[self setNeedsDisplay];
}

@end
