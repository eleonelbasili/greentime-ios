//
//  GTClientCellTextView.h
//  GreenTime
//
//  Created by Eduardo Dias on 9/09/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GTFontLabel.h"
#import "GTEnumBorders.h"

@interface GTTextView : UITextView <UIScrollViewDelegate>

@property(nonatomic, weak) id <UITextInput> nextTextInput;
@property(nonatomic, weak) id <UITextInput> previousTextInput;
@property(nonatomic)int borders;

-(void)setPlaceHolderText:(NSString *)text;

@end