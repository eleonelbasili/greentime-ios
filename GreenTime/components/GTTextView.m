//
//  GTClientCellTextView.m
//  GreenTime
//
//  Created by Eduardo Dias on 9/09/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import "GTTextView.h"

@implementation GTTextView
{
	GTFontLabel * _placeHolder;
	UIView * _bottomBorder;
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if (self)
	{
		self.font = [UIFont fontWithName:kGTFont size:kGTFontSizeMedium];
		[self makePlaceHolder];
		
		NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
        [nc addObserver:self
               selector:@selector(textDidChange:)
                   name:UITextViewTextDidChangeNotification
                 object:nil];
		
		_bottomBorder = [[UIView alloc] initWithFrame:CGRectMake(0, self.bounds.size.height - 0.5, self.bounds.size.width, 0.5)];
		_bottomBorder.backgroundColor = [UIColor gtGrey];
		_bottomBorder.hidden = true;
		[self addSubview:_bottomBorder];
		[self insertSubview:_bottomBorder belowSubview:self];
	}
	return self;
}

-(void)drawRect:(CGRect)rect
{
	[super drawRect:rect];
	
	CGContextRef context = UIGraphicsGetCurrentContext();
	CGContextSetShouldAntialias(context, NO);
	CGContextSetStrokeColorWithColor(context, [UIColor gtGrey].CGColor);
	CGContextSetLineWidth(context, 0.5f);
	
	if (_borders & BorderBottom)
	{
		CGContextMoveToPoint(context, 0.0f, self.frame.size.height);
		CGContextAddLineToPoint(context, rect.size.width, rect.size.height);
	}
	CGContextStrokePath(context);
}

-(void)dealloc
{
	NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
	[nc removeObserver:self];
}

- (void)setText:(NSString *)text
{
	[super setText:text];
	_placeHolder.hidden = (self.text.length > 0);
}

- (void)textDidChange:(NSNotification *)note
{
    _placeHolder.hidden = (self.text.length > 0);
}

-(void)setPlaceHolderText:(NSString *)text
{
	_placeHolder.text = text;
	_placeHolder.hidden = self.text.length > 0;
}

-(void)makePlaceHolder
{
	_placeHolder = [[GTFontLabel alloc] initWithFrame:CGRectMake(5, 0, self.frame.size.width, 30)];
	_placeHolder.font = self.font;
	_placeHolder.textColor = [UIColor gtGrey];
	[self addSubview:_placeHolder];
}

-(void)setBorders:(int)borders
{
	_borders = borders;
	[self setNeedsDisplay];
}

@end
