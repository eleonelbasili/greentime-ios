//
//  GTToolbarUtil.h
//  GreenTime
//
//  Created by Eduardo Dias on 4/09/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GTToolBarUtil : UIToolbar

@property(strong, nonatomic) UIBarButtonItem * previousButton;
@property(strong, nonatomic) UIBarButtonItem * nextButton;

-(UIToolbar *)initWithSelector:(id)target done:(SEL)done;
-(UIToolbar *)initWithSelectors:(id)target previous:(SEL)previous next:(SEL)next done:(SEL)done;

@end
