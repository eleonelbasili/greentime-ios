//
//  GTToolbarUtil.m
//  GreenTime
//
//  Created by Eduardo Dias on 4/09/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import "GTToolBarUtil.h"

@implementation GTToolBarUtil

-(UIToolbar *)initWithSelector:(id)target done:(SEL)done
{
	self = [super init];
	
	if (self)
	{
		self.backgroundColor = [UIColor gtLightGrey];
		[self sizeToFit];
		
		NSDictionary * attributes = [NSDictionary
									 dictionaryWithObjectsAndKeys:
									 [UIFont fontWithName:kGTFont size:kGTFontSizeMedium],
									 NSFontAttributeName, nil];
		
		UIBarButtonItem * space = [[UIBarButtonItem alloc]
								   initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
								   target:self action:nil];
		
		UIBarButtonItem * doneButton =[[UIBarButtonItem alloc]
									   initWithBarButtonSystemItem:UIBarButtonSystemItemDone
									   target:target action:done];
		[doneButton setTitleTextAttributes:attributes forState:UIControlStateNormal];
		[doneButton setTintColor:[UIColor gtGreen]];
		
		NSArray *items = @[space, doneButton];
		[self setItems:items];
	}
	return self;
}

-(UIToolbar *)initWithSelectors:(id)target
					   previous:(SEL)previous
						   next:(SEL)next
						   done:(SEL)done;
{
	self = [super init];
	
	if (self)
	{
		self.barTintColor = [UIColor gtLightGrey];
		self.translucent = NO;
		[self sizeToFit];
		
		NSDictionary * attributes = [NSDictionary
									 dictionaryWithObjectsAndKeys:
									 [UIFont fontWithName:kGTFontBold size:kGTFontSizeMedium],
									 NSFontAttributeName, nil];
		
		UIButton * pButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
		[pButton addTarget:target action:previous forControlEvents:UIControlEventTouchUpInside];
		[pButton setImage:[UIImage imageNamed:@"nav_left_arrow.png"] forState:UIControlStateNormal];
		[pButton setImageEdgeInsets:UIEdgeInsetsMake(0, -5, 0, 0)];
		self.previousButton =[[UIBarButtonItem alloc] initWithCustomView:pButton];
		[self.previousButton setTintColor:[UIColor gtGreen]];
		
		UIButton * nButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
		[nButton addTarget:target action:next forControlEvents:UIControlEventTouchUpInside];
		[nButton setImage:[UIImage imageNamed:@"nav_right_arrow.png"] forState:UIControlStateNormal];
		[nButton setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
		self.nextButton =[[UIBarButtonItem alloc] initWithCustomView:nButton];
		[self.nextButton setTintColor:[UIColor gtGreen]];
		
		
		UIBarButtonItem * leftSpace = [[UIBarButtonItem alloc]
									   initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
									   target:self action:nil];
		leftSpace.width = -14;
		
		UIBarButtonItem * rightSpace = [[UIBarButtonItem alloc]
										initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
										target:self action:nil];
		
		
		UIBarButtonItem * doneButton =[[UIBarButtonItem alloc]
									   initWithBarButtonSystemItem:UIBarButtonSystemItemDone
									   target:target action:done];
		[doneButton setTitleTextAttributes:attributes forState:UIControlStateNormal];
		[doneButton setTintColor:[UIColor gtGreen]];
		
		
		NSArray *items = @[leftSpace, self.previousButton, self.nextButton, rightSpace, doneButton];
		[self setItems:items];
		
		
		UIView * bottomLine = [[UIView alloc]
							   initWithFrame:CGRectMake(0, self.bounds.size.height - 0.5, self.bounds.size.width, 0.5)];
		bottomLine.backgroundColor = [UIColor gtGrey];
		
		[self addSubview:bottomLine];
		
	}
	return self;
}


@end
