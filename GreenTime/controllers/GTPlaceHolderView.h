//
//  GTEmptyTableView.h
//  GreenTime
//
//  Created by Eduardo Dias on 4/12/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString * const kGTClientPlaceHolder;
extern NSString * const kGTTimesheetPlaceHolder;

@interface GTPlaceHolderView : UIView

-(id)initWithFrame:(CGRect)frame viewType:(NSString *)viewType;

@end
