//
//  GTEmptyTableView.m
//  GreenTime
//
//  Created by Eduardo Dias on 4/12/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import "GTPlaceHolderView.h"

NSString * const kGTClientPlaceHolder = @"ClientPlaceHolder";
NSString * const kGTTimesheetPlaceHolder = @"TimesheetPlaceHolder";

@implementation GTPlaceHolderView
{
	NSDictionary * _viewDataArray;
}

-(id)initWithFrame:(CGRect)frame viewType:(NSString *)viewType
{
	self = [super initWithFrame:frame];
	if (self)
	{
		self.backgroundColor = [UIColor gtWhite];
		_viewDataArray = @{kGTClientPlaceHolder:@[@"client_bg",NSLocalizedString(@"placeholder.client", nil)],
					    kGTTimesheetPlaceHolder:@[@"timesheet_bg",NSLocalizedString(@"placeholder.timesheet", nil)]};
		
		NSArray * viewData = [_viewDataArray valueForKey:viewType];
		if (viewData)
		{
			UIImage * image = [UIImage imageNamed:viewData[0]];
			float imageXPos = frame.size.width / 2 - image.size.width / 2;
			float imageYPos = frame.size.height / 2 - image.size.height;
			
			if (viewType == kGTTimesheetPlaceHolder)
			{
				imageXPos -= 20;
				imageYPos -= 20;
			}
			
			CGRect imageViewFrame = CGRectMake(imageXPos, imageYPos, image.size.width, image.size.height);
			UIImageView * imageView = [[UIImageView alloc] initWithFrame:imageViewFrame];
			[imageView setImage:image];
			
			[self addSubview:imageView];
			
			
			float labelGap = 10;
			float labelYPos = imageYPos + image.size.height + labelGap;
			
			CGRect labelFrame = CGRectMake(labelGap, labelYPos, frame.size.width - labelGap * 2, 20);
			GTFontLabel * label = [[GTFontLabel alloc] initWithFrame:labelFrame];
			label.textColor = [UIColor gtGrey];
			label.textAlignment = NSTextAlignmentCenter;
			label.text = viewData[1];
			
			[self addSubview:label];
		}
	}
	return self;
}


@end
