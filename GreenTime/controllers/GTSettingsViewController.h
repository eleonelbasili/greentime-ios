//
//  GTSettingsViewController.h
//  GreenTime
//
//  Created by Eduardo Dias on 3/01/14.
//  Copyright (c) 2014 com.dymextech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GTFontLabel.h"

@interface GTSettingsViewController : UITableViewController

@property (strong, nonatomic) IBOutlet UISegmentedControl * timeFormatSwitch;
@property (strong, nonatomic) IBOutlet GTFontLabel * logoutLabel;
@property (strong, nonatomic) IBOutlet UILabel * availableLocationsLabel;
@property (strong, nonatomic) IBOutlet UISegmentedControl * dateFormatSwitch;

- (IBAction)setTimeFormat:(id)sender;
- (IBAction)setDateFormat:(id)sender;

@end
