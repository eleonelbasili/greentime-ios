//
//  GTSettingsViewController.m
//  GreenTime
//
//  Created by Eduardo Dias on 3/01/14.
//  Copyright (c) 2014 com.dymextech. All rights reserved.
//

#import "GTSettingsViewController.h"
#import "GTLocationManager.h"
#import <Parse/Parse.h>

@interface GTSettingsViewController ()

@end

@implementation GTSettingsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.logoutLabel.textColor = [UIColor gtRed];
	
	// Updating display seconds switch
	NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
	NSString * displaySeconds = [userDefaults objectForKey:kGTTimesheetReportDisplaySeconds];
	displaySeconds = displaySeconds ? displaySeconds : @"Y";

	NSInteger timeIndex = [displaySeconds boolValue] ? 1 : 0;
	[self.timeFormatSwitch setSelectedSegmentIndex:timeIndex];
	
	NSString * preferredTimeFormat = [userDefaults objectForKey:kGTPreferredDateFormat];
	preferredTimeFormat = preferredTimeFormat ? preferredTimeFormat : kGTFullDateFormat;
	
	NSInteger dateIndex = [preferredTimeFormat isEqualToString:kGTCompactDateFormat] ? 0 : 1;
	[self.dateFormatSwitch setSelectedSegmentIndex:dateIndex];
}

- (void)viewWillAppear:(BOOL)animated
{
	[self updatingAvailableLocations];
}

- (void) updatingAvailableLocations
{
	int availableLocations = [[GTLocationManager instance] availableLocations];
	NSString * locationsAvailableText = [NSString stringWithFormat:@"%d of %d",
										 availableLocations, kGTMaxLocationsAvailable];
	
	// Updating available locations
	NSMutableAttributedString * labelAttributes = [[NSMutableAttributedString alloc] initWithString:locationsAvailableText];
	[labelAttributes addAttribute:NSForegroundColorAttributeName
							value:[UIColor gtGreen]
							range:NSMakeRange(0, 2)];
	[labelAttributes addAttribute:NSForegroundColorAttributeName
							value:[UIColor gtGreen]
							range:NSMakeRange(6, 2)];
	[self.availableLocationsLabel setAttributedText:labelAttributes];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.section == 2 && indexPath.row == 0)
	{
		[PFUser logOut];
		UIViewController * authenticationViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AuthenticationController"];
		[self presentViewController:authenticationViewController animated:YES completion:nil];
	}
	else if (indexPath.section == 0 && indexPath.row == 0)
	{
		[[GTLocationManager instance]stopMonitoringRegions];
		[self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];
		[self performSelector:@selector(updatingAvailableLocations) withObject:self afterDelay:0.5];
	}
}

- (IBAction)setTimeFormat:(id)sender
{
	NSString * displaySeconds = (self.timeFormatSwitch.selectedSegmentIndex == 1) ? @"Y" : @"N";
	NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
	[userDefaults setObject:displaySeconds forKey:kGTTimesheetReportDisplaySeconds];
	[userDefaults synchronize];
}

- (IBAction)setDateFormat:(id)sender
{
	NSString * preferredTimeFormat = (self.dateFormatSwitch.selectedSegmentIndex == 0) ? kGTCompactDateFormat : kGTFullDateFormat;
	NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
	[userDefaults setObject:preferredTimeFormat forKey:kGTPreferredDateFormat];
	[userDefaults synchronize];
}
@end
