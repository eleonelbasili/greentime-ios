//
//  GTTabBarController.m
//  GreenTime
//
//  Created by Eduardo Dias on 7/11/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import "GTTabBarController.h"
#import "GTLocationManager.h"
#import "GTRecordTimeViewController.h"

NSString * const kGTRecordingNavigationViewController = @"RecordingNavigationViewController";

@interface GTTabBarController ()

@end

@implementation GTTabBarController

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
	{
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(didEnterRegion:)
													 name:kGTDidEnterRegion
												   object:nil];
    }
    return self;
}

-(void)didEnterRegion:(NSNotification *)notification
{
	NSArray * viewControllers = [self viewControllers];
	NSArray * viewControllersFiltered = [viewControllers filteredArrayUsingPredicate:
										 [NSPredicate predicateWithFormat:@"self.restorationIdentifier == %@", kGTRecordingNavigationViewController ]];
	if (viewControllersFiltered.count > 0)
	{
		UIViewController * recordingNavViewController = [viewControllersFiltered objectAtIndex:0];
		
		NSUInteger recordingNavViewControllerIndex = [viewControllers indexOfObject:recordingNavViewController];
		self.selectedIndex = recordingNavViewControllerIndex;
	}
}

@end
