//
//  GTAuthenticationViewController.h
//  GreenTime
//
//  Created by Eduardo Dias on 27/11/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface GTAuthenticationViewController : UIViewController <PFSignUpViewControllerDelegate, PFLogInViewControllerDelegate>

@end
