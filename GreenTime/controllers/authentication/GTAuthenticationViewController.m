//
//  GTAuthenticationViewController.m
//  GreenTime
//
//  Created by Eduardo Dias on 27/11/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import "GTAuthenticationViewController.h"
#import "GTLogInViewController.h"
#import "GTSignUpViewController.h"
#import "GTTabBarController.h"
#import <Parse/Parse.h>

@interface GTAuthenticationViewController ()

@end

@implementation GTAuthenticationViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}

-(void)viewDidAppear:(BOOL)animated
{
	//[PFUser logOut];
	if (![PFUser currentUser])
	{
		GTLogInViewController * logInController = [[GTLogInViewController alloc] init];
		logInController.fields =  PFLogInFieldsUsernameAndPassword |
		PFLogInFieldsLogInButton | PFLogInFieldsFacebook | PFLogInFieldsTwitter | PFLogInFieldsSignUpButton;
		logInController.facebookPermissions = @[@"friends_about_me"];
		[logInController setDelegate:self];
		
		GTSignUpViewController * signUpViewController = [[GTSignUpViewController alloc] init];
		[signUpViewController setDelegate:self];
		
		[logInController setSignUpController:signUpViewController];
		
		[self presentViewController:logInController animated:YES completion:NULL];
	}
	else
	{
		GTTabBarController * tabBarController = [self.storyboard instantiateViewControllerWithIdentifier:@"TabBarController"];
		
		[self presentViewController:tabBarController animated:YES completion:NULL];
	}
}


#pragma mark - Login Operations

-(BOOL)logInViewController:(PFLogInViewController *)logInController shouldBeginLogInWithUsername:(NSString *)username password:(NSString *)password
{
    if (username && password && username.length && password.length)
	{
        return YES;
    }
    
    [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Missing Information", nil)
								message:NSLocalizedString(@"Make sure you fill out all of the information!", nil)
							   delegate:nil
					  cancelButtonTitle:NSLocalizedString(@"OK", nil)
					  otherButtonTitles:nil] show];
    return NO;
}

-(void)logInViewController:(PFLogInViewController *)logInController didLogInUser:(PFUser *)user
{
	[self dismissViewControllerAnimated:NO completion:^
	{
		GTTabBarController * tabBarController = [self.storyboard instantiateViewControllerWithIdentifier:@"TabBarController"];
		[self presentViewController:tabBarController animated:YES completion:NULL];
	}];
}

-(void)logInViewController:(PFLogInViewController *)logInController didFailToLogInWithError:(NSError *)error
{
	NSLog(@"Error: %@ %@", error, [error userInfo]);
}

-(void)logInViewControllerDidCancelLogIn:(PFLogInViewController *)logInController
{
	
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - Signup Operations

// Sent to the delegate to determine whether the sign up request should be submitted to the server.
- (BOOL)signUpViewController:(PFSignUpViewController *)signUpController shouldBeginSignUp:(NSDictionary *)info
{
    BOOL informationComplete = YES;
	
    // loop through all of the submitted data
    for (id key in info)
	{
        NSString *field = [info objectForKey:key];
        if (!field || field.length == 0)
		{
            informationComplete = NO;
            break;
        }
    }
	
    if (!informationComplete)
	{
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"authentication.missingInfo.title", nil)
									message:NSLocalizedString(@"authentication.missingInfo.message", nil)
								   delegate:nil
						  cancelButtonTitle:@"ok"
						  otherButtonTitles:nil] show];
    }
	
    return informationComplete;
}

// Sent to the delegate when a PFUser is signed up.
- (void)signUpViewController:(PFSignUpViewController *)signUpController didSignUpUser:(PFUser *)user
{
	[self dismissViewControllerAnimated:NO completion:^
	 {
		 GTTabBarController * tabBarController = [self.storyboard instantiateViewControllerWithIdentifier:@"TabBarController"];
		 [self presentViewController:tabBarController animated:YES completion:NULL];
	 }];
}

// Sent to the delegate when the sign up attempt fails.
- (void)signUpViewController:(PFSignUpViewController *)signUpController didFailToSignUpWithError:(NSError *)error
{
    NSLog(@"Failed to sign up...");
}

// Sent to the delegate when the sign up screen is dismissed.
- (void)signUpViewControllerDidCancelSignUp:(PFSignUpViewController *)signUpController
{
    NSLog(@"User dismissed the signUpViewController");
}

@end
