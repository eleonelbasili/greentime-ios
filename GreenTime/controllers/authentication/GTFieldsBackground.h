//
//  GTLoginFieldsView.h
//  GreenTime
//
//  Created by Eduardo Dias on 29/11/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GTFieldsBackground : UIView

@property(nonatomic)int numFields;

@end
