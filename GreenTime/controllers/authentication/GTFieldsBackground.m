//
//  GTLoginFieldsView.m
//  GreenTime
//
//  Created by Eduardo Dias on 29/11/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import "GTFieldsBackground.h"

@implementation GTFieldsBackground

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
	{
		self.backgroundColor = [UIColor gtWhite];
		self.layer.masksToBounds = true;
		self.layer.cornerRadius = 5.0;
    }
    return self;
}

- (void)setNumFields:(int)numFields
{
	_numFields = numFields;
	[self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect
{
	CGContextRef context = UIGraphicsGetCurrentContext();
	
	CGFloat gapHeight = rect.size.height / (self.numFields);
	for (int i = 0; i < self.numFields; i++)
	{
		CGFloat yPos = (i * gapHeight) + gapHeight;
		CGContextSetStrokeColorWithColor(context, [UIColor gtGrey].CGColor);
		CGContextSetLineWidth(context, 0.5);
		CGContextMoveToPoint(context, 10, yPos);
		CGContextAddLineToPoint(context, rect.size.width - 20, yPos);
	}
	
	CGContextStrokePath(context);
}


@end
