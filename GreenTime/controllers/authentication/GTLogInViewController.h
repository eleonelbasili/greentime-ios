//
//  GTLogInViewController.h
//  GreenTime
//
//  Created by Eduardo Dias on 29/11/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import <Parse/Parse.h>
#import "GTFieldsBackground.h"

@interface GTLogInViewController : PFLogInViewController

@end
