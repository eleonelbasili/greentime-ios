//
//  GTLogInViewController.m
//  GreenTime
//
//  Created by Eduardo Dias on 29/11/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import "GTLogInViewController.h"
#import "GTFontLabel.h"

@interface GTLogInViewController ()
{
	GTFieldsBackground * _fieldsBackground;
}
@end

@implementation GTLogInViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	// Customizing background color
	[self.logInView setBackgroundColor:[UIColor gtGreen]];
	
	// Customizing background fields view
	_fieldsBackground = [[GTFieldsBackground alloc] initWithFrame:CGRectMake(0,0,0,0)];
	_fieldsBackground.numFields = 2;
    [self.logInView insertSubview:_fieldsBackground atIndex:1];
	
	// Customizaing logo
	[self.logInView setLogo:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo.png"]]];
	
	// Customizing textfields
	self.logInView.usernameField.font = [UIFont fontWithName:kGTFont size:kGTFontSizeMedium];
	self.logInView.usernameField.textColor = [UIColor gtGreen];
	self.logInView.usernameField.placeholder = NSLocalizedString(@"login.usernamePlaceHolder", nil);
	self.logInView.usernameField.layer.shadowOpacity = 0.0;
	
	self.logInView.passwordField.font = [UIFont fontWithName:kGTFont size:kGTFontSizeMedium];
	self.logInView.passwordField.textColor = [UIColor gtGreen];
	self.logInView.passwordField.placeholder = NSLocalizedString(@"login.passwordPlaceHolder", nil);
	self.logInView.passwordField.layer.shadowOpacity = 0.0;
	
	// Customizing login button
	[self.logInView.logInButton setBackgroundColor:[UIColor gtDarkGreen]];
	self.logInView.logInButton.titleLabel.font = [UIFont fontWithName:kGTFontBold size:kGTFontSizeMedium];
	[self.logInView.logInButton setBackgroundImage:nil forState:UIControlStateNormal];
	[self.logInView.logInButton setBackgroundImage:nil forState:UIControlStateHighlighted];
	self.logInView.logInButton.layer.cornerRadius = 5.0;
	self.logInView.logInButton.titleLabel.shadowOffset =  CGSizeMake(0.0, 0.0);
	
	// Customizing labels
	for (UIView * view in self.logInView.subviews)
	{
		if ([view isKindOfClass:[UILabel class]])
		{
			UILabel * label = (UILabel *)view;
			label.textColor = [UIColor gtWhite];
			label.shadowOffset =  CGSizeMake(0.0, 0.0);
		}
	}
	
	// Customizing facebook button
	[self.logInView.facebookButton setImage:nil forState:UIControlStateNormal];
    [self.logInView.facebookButton setImage:nil forState:UIControlStateHighlighted];
    [self.logInView.facebookButton setBackgroundImage:[UIImage imageNamed:@"facebook_btn.png"] forState:UIControlStateHighlighted];
    [self.logInView.facebookButton setBackgroundImage:[UIImage imageNamed:@"facebook_btn.png"] forState:UIControlStateNormal];
    [self.logInView.facebookButton setTitle:@"" forState:UIControlStateNormal];
    [self.logInView.facebookButton setTitle:@"" forState:UIControlStateHighlighted];
	
	// Customizing twitter button
	[self.logInView.twitterButton setImage:nil forState:UIControlStateNormal];
    [self.logInView.twitterButton setImage:nil forState:UIControlStateHighlighted];
    [self.logInView.twitterButton setBackgroundImage:[UIImage imageNamed:@"twitter_btn.png"] forState:UIControlStateHighlighted];
    [self.logInView.twitterButton setBackgroundImage:[UIImage imageNamed:@"twitter_btn.png"] forState:UIControlStateNormal];
    [self.logInView.twitterButton setTitle:@"" forState:UIControlStateNormal];
    [self.logInView.twitterButton setTitle:@"" forState:UIControlStateHighlighted];
	
	// Customizing signUp button
	[self.logInView.signUpButton setBackgroundColor:[UIColor gtBlack]];
	self.logInView.signUpButton.titleLabel.font = [UIFont fontWithName:kGTFontBold size:kGTFontSizeMedium];
	[self.logInView.signUpButton setBackgroundImage:nil forState:UIControlStateNormal];
	[self.logInView.signUpButton setBackgroundImage:nil forState:UIControlStateHighlighted];
	self.logInView.signUpButton.layer.cornerRadius = 5.0;
	self.logInView.signUpButton.titleLabel.shadowOffset =  CGSizeMake(0.0, 0.0);
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
	
	CGFloat viewHeight = self.view.frame.size.height;
	CGFloat viewWidth = self.view.frame.size.width;
	
	[self.logInView.logo setFrame:CGRectMake((viewWidth / 2) - 37.5f, viewHeight - 450, 75.0f, 75.0f)];
	[_fieldsBackground setFrame:CGRectMake(35, viewHeight - 365, viewWidth - 70, 90)];
	
	[self.logInView.logInButton setTitleColor:[UIColor gtWhite] forState:UIControlStateNormal];
	[self.logInView.logInButton setBackgroundImage:nil forState:UIControlStateHighlighted];
	[self.logInView.logInButton setFrame:CGRectMake(35.0f, viewHeight - 265, 250.0f, 50.0f)];
	[self.logInView.facebookButton setFrame:CGRectMake(35.0f,  viewHeight - 172, 120.0f, 50.0f)];
	[self.logInView.twitterButton setFrame:CGRectMake(165.0f, viewHeight - 172, 120.0f, 50.0f)];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
