//
//  GTSetupViewController.h
//  GreenTime
//
//  Created by Eduardo Dias on 30/11/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "GTFieldsBackground.h"

@interface GTSignUpViewController : PFSignUpViewController

@end
