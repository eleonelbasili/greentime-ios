//
//  GTSetupViewController.m
//  GreenTime
//
//  Created by Eduardo Dias on 30/11/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import "GTSignUpViewController.h"
#import "GTFieldsBackground.h"

@interface GTSignUpViewController ()

@end

@implementation GTSignUpViewController
{
	GTFieldsBackground * _fieldsBackgroundView;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
	{
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	// Customizing background color
	[self.signUpView setBackgroundColor:[UIColor gtGreen]];
	
	// Customizing logo
	[self.signUpView setLogo:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo.png"]]];

	// Customizing background fields view
	_fieldsBackgroundView = [[GTFieldsBackground alloc] initWithFrame:CGRectMake(0, 0, 246, 130)];
	_fieldsBackgroundView.numFields = 3;
	CGSize viewSize = self.view.frame.size;
	[_fieldsBackgroundView setCenter:CGPointMake(viewSize.width / 2, viewSize.height / 2)];
    [self.signUpView insertSubview:_fieldsBackgroundView atIndex:2];
	
	// Customizing username fields
	self.signUpView.usernameField.font = [UIFont fontWithName:kGTFont size:kGTFontSizeMedium];
	self.signUpView.usernameField.textColor = [UIColor gtGreen];
	self.signUpView.usernameField.layer.shadowOpacity = 0.0;
	
	// Customizing passwords fields
	self.signUpView.passwordField.font = [UIFont fontWithName:kGTFont size:kGTFontSizeMedium];
	self.signUpView.passwordField.textColor = [UIColor gtGreen];
	self.signUpView.passwordField.layer.shadowOpacity = 0.0;
	
	// Customizing signup fields
	self.signUpView.emailField.font = [UIFont fontWithName:kGTFont size:kGTFontSizeMedium];
	self.signUpView.emailField.textColor = [UIColor gtGreen];
	self.signUpView.emailField.layer.shadowOpacity = 0.0;
	
	// Customizing signUp button
	[self.signUpView.signUpButton setBackgroundColor:[UIColor gtDarkGreen]];
	[self.signUpView.signUpButton setBackgroundImage:nil forState:UIControlStateNormal];
	self.signUpView.signUpButton.layer.cornerRadius = 5.0;
	self.signUpView.signUpButton.titleLabel.shadowOffset = CGSizeMake(0.0, 0.0);
}

-(void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:YES];
	[self.signUpView.usernameField becomeFirstResponder];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
	[self.signUpView.dismissButton increaseY:15];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
