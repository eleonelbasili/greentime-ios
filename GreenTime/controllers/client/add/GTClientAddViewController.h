//
//  GTClientViewController.h
//  GreenTime
//
//  Created by Eduardo Dias on 30/08/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GTFormTableViewController.h"
#import "GTClientHeaderCell.h"
#import "Client.h"
#import "Project.h"

@class GTClientHeaderCell;

@interface GTClientAddViewController : GTFormTableViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIActionSheetDelegate, UIAlertViewDelegate>

@property(nonatomic, strong)Client * client;

- (IBAction)save:(id)sender;

@end