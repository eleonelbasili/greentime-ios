//
//  GTClientViewController.m
//  GreenTime
//
//  Created by Eduardo Dias on 30/08/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import "GTProjectViewController.h"
#import "GTClientServices.h"
#import "GTProjectServices.h"
#import "UIImage+ScaleToFit.h"
#import "GTTextView.h"
#import "GTClientHeaderCell.h"
#import "ClientModel.h"
#import <Parse/Parse.h>
#import <ReactiveCocoa/ReactiveCocoa.h>

static CGFloat const HEADER_CELL_HEIGHT = 328;
static CGFloat const DELETE_CELL_HEIGHT = 80;

#define TOTAL_ROWS (1 + [self.client projectsCount] + DISPLAY_DELETE_BUTTON)
#define DISPLAY_DELETE_BUTTON ((self.client.objectId == nil) ? 0 : 1) // If client is saved, show delete button

@interface GTClientAddViewController ()
@end

@implementation GTClientAddViewController
{
	GTClientHeaderCell * _headerCell;
	Project * _selectedProject;
	UIActionSheet * _actionSheet;
	PFFile * _imageFile;
	BOOL _isDataBinded;
}


#pragma mark - Initialization

-(void)viewDidLoad
{
    [super viewDidLoad];
	
	if (self.client == nil)
	{
		Client * client = [[Client alloc] init];
		client.user = [PFUser currentUser];
		self.client = client;
	}
	else
	{
		self.title = @"";
		[self loadProjects];
	}
	// Removing separator to empty rows
	UIView *view = [[UIView alloc] init];
	view.backgroundColor = [UIColor clearColor];
	[self.tableView setTableFooterView:view];
}

-(void)loadProjects
{
	[[GTProjectServices instance] findProjectsWithClientId:self.client.objectId
												  block:^(NSArray * projects, NSError *error)
	 {
		 if (!error)
		 {
			 [self.client addProjects:projects];
			 [self.tableView reloadData];
		 }
	 }];
}

-(void)viewWillAppear:(BOOL)animated
{
	_selectedProject = nil;
	[self.tableView reloadData];
}

-(void)viewDidAppear:(BOOL)animated
{
	if (!_isDataBinded)
	{
		// Binding data
		RACChannelTerminal * nameBind = RACChannelTo(self.client, name);
		RAC(_headerCell.nameTextfield, text) = nameBind;
		[[_headerCell.nameTextfield.rac_textSignal filter:^BOOL(NSString * value)
		{
			return ![value isEqualToString:@""];
		}]subscribe:nameBind];
		
		RACChannelTerminal * phoneBind = RACChannelTo(self.client, phone);
		RAC(_headerCell.phoneTextfield, text) = phoneBind;
		[[_headerCell.phoneTextfield.rac_textSignal filter:^BOOL(NSString * value)
		{
			return ![value isEqualToString:@""];
		}]subscribe:phoneBind];
		
		RACChannelTerminal * emailBind = RACChannelTo(self.client, email);
		RAC(_headerCell.emailTextfield, text) = emailBind;
		[[_headerCell.emailTextfield.rac_textSignal filter:^BOOL(NSString * value)
		{
			return ![value isEqualToString:@""];
		}]subscribe:emailBind];
		
		RACChannelTerminal * websiteBind = RACChannelTo(self.client, website);
		RAC(_headerCell.websiteTextfield, text) = websiteBind;
		[[_headerCell.websiteTextfield.rac_textSignal filter:^BOOL(NSString * value)
		{
			return ![value isEqualToString:@""];
		}]subscribe:websiteBind];
		
		_headerCell.notesTextfield.text = self.client.notes;
		
		[RACObserve(_headerCell.addPhotoButton.imageView, image) subscribeNext:^(UIImage * image)
		 {
			 if (image)
			 {
				 NSData * imageData = UIImageJPEGRepresentation(image, 1.0f);
				 self.client.image = [PFFile fileWithData:imageData];
			 }
		 }];
		
		[[RACObserve(self.client, image) take:1] subscribeNext:^(PFFile * pfFile)
		 {
			 if (pfFile)
			 {
				 dispatch_queue_t filterQueue = dispatch_queue_create("com.greentime.displayImage", NULL);
				 dispatch_async(filterQueue, ^
				 {
					 NSData * imageData = [pfFile getData];
					 UIImage * image = [UIImage imageWithData:imageData];
					 if (image)
					 {
						 dispatch_async(dispatch_get_main_queue(), ^
						 {
							 [_headerCell.addPhotoButton setImage:image forState:UIControlStateNormal];
						 });
					 }
				 });
			 }
		 }];
		_isDataBinded = true;
	}
}

#pragma mark - Action Sheet Operations

-(void)showActionSheet:(id)sender
{
	_actionSheet = [[UIActionSheet alloc]
				   initWithTitle:nil
				   delegate:self
				   cancelButtonTitle:NSLocalizedString(@"cancel", nil)
				   destructiveButtonTitle:nil
				   otherButtonTitles:NSLocalizedString(@"client.actionSheet.takePicture", nil),
				   NSLocalizedString(@"client.actionSheet.chooseFromLibrary", nil), nil];
	
	[_actionSheet showInView:[self.view window]];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if (buttonIndex < 2)
	{
		UIImagePickerController *picker = [[UIImagePickerController alloc] init];
		picker.delegate = self;
		picker.allowsEditing = YES;
		picker.sourceType = (buttonIndex == 0) ? UIImagePickerControllerSourceTypeCamera : UIImagePickerControllerSourceTypePhotoLibrary;
		
		[self presentViewController:picker animated:YES completion:NULL];
	}
}

-(UIImage *)roundedImage:(UIImage *) image
{
	CGSize newSize = _headerCell.addPhotoButton.bounds.size;
	UIImage * scaledImage = [image scaleToFit:CGSizeMake(newSize.width, newSize.height)];
	CGRect scaledImageRect = CGRectMake(0, 0, scaledImage.size.width, scaledImage.size.height);
	
	UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
	
	UIBezierPath *path = [UIBezierPath bezierPathWithOvalInRect:scaledImageRect];
	[path addClip];
	[image drawInRect:scaledImageRect];
	
	CGContextRef ctx = UIGraphicsGetCurrentContext();
	CGContextSetStrokeColorWithColor(ctx, [[UIColor gtGreen] CGColor]);
	[path setLineWidth:5.0f];
	[path stroke];
	
	UIImage *roundedImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	return roundedImage;
}


 -(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
 {
	 [picker dismissViewControllerAnimated:YES completion:^{
		 UIImage *image = info[UIImagePickerControllerEditedImage];
		 UIImage *roundedImage = [self roundedImage:image];
		 [_headerCell.addPhotoButton setImage:roundedImage forState:UIControlStateNormal ];
	 }];
 }


#pragma mark - TableView Operations

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	CGFloat cellHeight;
	if (indexPath.row == 0)
	{
		cellHeight = HEADER_CELL_HEIGHT;
	}
	else if (indexPath.row > 0 && indexPath.row <= [self.client projectsCount])
	{
		cellHeight = tableView.rowHeight;
	}
	else
	{
		cellHeight = DELETE_CELL_HEIGHT;
	}
	return cellHeight;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return TOTAL_ROWS;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	NSString * headerCellID  = @"headerCell";
	NSString * projectCellID = @"projectCell";
	NSString * buttonCellID  = @"buttonCell";
	
	UITableViewCell * cell;
	
	if (indexPath.row == 0)
	{
		_headerCell = (GTClientHeaderCell *) [tableView dequeueReusableCellWithIdentifier:headerCellID];
		
		if (_headerCell)
		{
			[_headerCell.addPhotoButton addTarget:self
										  action:@selector(showActionSheet:)
								forControlEvents:UIControlEventTouchDown];
			
			[self setupTextFieldNavigation:_headerCell.nameTextfield
								   forward:_headerCell.phoneTextfield backward:nil];
			
			[self setupTextFieldNavigation:_headerCell.phoneTextfield
								   forward:_headerCell.emailTextfield backward:_headerCell.nameTextfield];
			
			[self setupTextFieldNavigation:_headerCell.emailTextfield
								   forward:_headerCell.websiteTextfield backward:_headerCell.phoneTextfield];
			
			[self setupTextFieldNavigation:_headerCell.websiteTextfield
								   forward:_headerCell.notesTextfield backward:_headerCell.emailTextfield];
			
			[self setupTextFieldNavigation:_headerCell.notesTextfield
								   forward:nil backward:_headerCell.websiteTextfield];
			cell = _headerCell;
		}
	}
	else if (indexPath.row > 0 && indexPath.row <= [self.client projectsCount])
	{
		cell = [tableView dequeueReusableCellWithIdentifier:projectCellID];
		if (cell == nil)
		{
			cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:projectCellID];
			[cell.textLabel setFont:[UIFont fontWithName:kGTFont size:kGTFontSizeMedium]];
			cell.textLabel.font = [UIFont fontWithName:kGTFont size:kGTFontSizeMedium];
			cell.autoresizingMask = UIViewAutoresizingFlexibleWidth;
			cell.shouldIndentWhileEditing = NO;
		}
		Project * project = [self.client projectAtIndex:indexPath.row - 1];
		cell.textLabel.text = project.name;
	}
	else
	{
		cell = [tableView dequeueReusableCellWithIdentifier:buttonCellID];
		if (cell == nil)
		{
			cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:buttonCellID];
			cell.autoresizingMask = UIViewAutoresizingFlexibleWidth;
			cell.shouldIndentWhileEditing = NO;
			cell.separatorInset  = UIEdgeInsetsMake(0, 10000, 0, 0);
			cell.selectionStyle  = UITableViewCellSelectionStyleNone;
			[cell.textLabel setFont:[UIFont fontWithName:kGTFont size:kGTFontSizeMedium]];
			
			CGFloat buttonYPos  = DELETE_CELL_HEIGHT / 2 - tableView.rowHeight / 2;
			CGFloat buttonWidth = cell.bounds.size.width - 30;
			CGRect buttonRect   = CGRectMake(15, buttonYPos, buttonWidth, tableView.rowHeight);
			
			UIButton * deleteButton = [[UIButton alloc] initWithFrame:buttonRect];
			[deleteButton.layer setCornerRadius:kGTCornerRadius];
			deleteButton.backgroundColor = [UIColor gtRed];
			[deleteButton setTitle:NSLocalizedString(@"client.deleteButton", nil)forState:UIControlStateNormal];
			[deleteButton addTarget:self
							 action:@selector(deleteClient:)
				   forControlEvents:UIControlEventTouchUpInside];
			
			[cell addSubview:deleteButton];
		}
	}

	return cell;
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
	return (indexPath.row > 0);
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (editingStyle == UITableViewCellEditingStyleDelete)
	{
		[self.client removeProjectAtIndex:indexPath.row - 1];
		[tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationTop];
	}
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.row > 0)
	{
		_selectedProject = [self.client projectAtIndex:indexPath.row - 1];
		[self performSegueWithIdentifier:@"ProjectViewController" sender:self];
	}
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if ([segue.identifier isEqualToString:@"ProjectViewController"])
	{
		GTProjectViewController * projectViewController = segue.destinationViewController;
		projectViewController.client = self.client;
		projectViewController.project = _selectedProject;
	}
}


#pragma mark - Textfield Operations

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
	if ([textField.restorationIdentifier isEqualToString:@"hourlyRate"])
	{
		bool isNumericCharacter = ([string rangeOfCharacterFromSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]].location == NSNotFound);
		
		if (isNumericCharacter)
		{
			NSString *currency = [[NSLocale currentLocale] objectForKey:NSLocaleCurrencySymbol];
			
			if (range.location == 1 && [string isEqualToString:@""])
			{
				textField.text = @"";
			}
			
			if (![textField.text rangeOfString:currency].length > 0)
			{
				textField.text = [textField.text stringByAppendingString:currency];
			}
			
			if ([textField.text rangeOfString:@"."].length > 0 && [string isEqualToString:@"."])
			{
				return NO;
			}
		}
		return isNumericCharacter;
	}
	return YES;
}

- (void)keyboardDidShow:(NSNotification *)notification
{
	[super keyboardDidShow:notification];
	
	CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
	[self.view decreaseHeight:keyboardSize.height - self.tabBarController.tabBar.height];
}

- (void)keyboardWillHide:(NSNotification *)notification
{
	[super keyboardWillHide:notification];
	
	CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
	[self.view increaseHeight:keyboardSize.height - self.tabBarController.tabBar.height];
}


#pragma mark - Save Operations

- (IBAction)save:(id)sender
{
	self.client.notes = _headerCell.notesTextfield.text;
	[self showHUD:MBProgressHUDModeIndeterminate label:NSLocalizedString(@"Saving", nil)];
	[[GTClientServices instance] saveClient:[self.client toDictionary]
									  block:^(Client * client, NSError *error)
	{
		 if (client)
		 {
			 [self.navigationController popViewControllerAnimated:YES];
		 }
		 [self hideHUD];
	}];
}


#pragma mark - Delete Operations

- (void)deleteClient:(UIButton *)sender
{
	if (self.client && ![[ClientModel instance]isClientInRecordingMode:self.client])
	{
		[[[UIAlertView alloc]
		  initWithTitle:NSLocalizedString(@"client.alertViewDelete.title", nil)
		  message:NSLocalizedString(@"client.alertViewDelete.message", nil)
		  delegate:self
		  cancelButtonTitle:NSLocalizedString(@"cancel", nil)
		  otherButtonTitles:NSLocalizedString(@"ok", nil), nil] show];
	}
	else
	{
		[[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"client.recordingMode.alertView.title", nil)
									message:NSLocalizedString(@"client.recordingMode.alertView.Message", nil)
								   delegate:self
						  cancelButtonTitle:NSLocalizedString(@"ok", nil)
						  otherButtonTitles:nil, nil] show];
	}
}


#pragma mark - Alert View Operations

- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex
{
	if (buttonIndex == 1)
	{
		[self showHUD:MBProgressHUDModeIndeterminate label:NSLocalizedString(@"Deleting", nil)];
		
		[[GTClientServices instance] deleteClient:self.client.objectId
											block:^(Client * client, NSError *error)
		 {
			 if (!error)
			 {
				 [self.navigationController popViewControllerAnimated:YES];
			 }
			 else
			 {
				 NSLog(@"Error: %@ %@", error, [error userInfo]);
			 }
			 [self hideHUD];
		 }];
	}
}

@end
