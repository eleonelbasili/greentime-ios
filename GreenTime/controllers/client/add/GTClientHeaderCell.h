//
//  GTClientHeaderCell.h
//  GreenTime
//
//  Created by Eduardo Dias on 30/08/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GTTextField.h"
#import "GTClientPhotoButton.h"
#import "GTTextView.h"
#import "GTClientAddViewController.h"

@interface GTClientHeaderCell : UITableViewCell

@property (strong, nonatomic) IBOutlet GTTextField * nameTextfield;
@property (strong, nonatomic) IBOutlet GTTextField * phoneTextfield;
@property (strong, nonatomic) IBOutlet GTTextField * emailTextfield;
@property (strong, nonatomic) IBOutlet GTClientPhotoButton * addPhotoButton;
@property (strong, nonatomic) IBOutlet GTTextField * websiteTextfield;
@property (strong, nonatomic) IBOutlet GTTextView * notesTextfield;

@end
