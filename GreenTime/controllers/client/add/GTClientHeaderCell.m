//
//  GTClientHeaderCell.m
//  GreenTime
//
//  Created by Eduardo Dias on 30/08/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import "GTClientHeaderCell.h"

@implementation GTClientHeaderCell

-(void)layoutSubviews
{
	[super layoutSubviews];
	
	self.nameTextfield.borders = BorderBottom;
	self.phoneTextfield.borders = BorderBottom;
	self.emailTextfield.borders = BorderBottom;
	self.websiteTextfield.borders = BorderBottom;
	
	[self.notesTextfield setPlaceHolderText:NSLocalizedString(@"client.headerCell.notes", nil)];
	self.notesTextfield.borders = BorderBottom;
}

@end