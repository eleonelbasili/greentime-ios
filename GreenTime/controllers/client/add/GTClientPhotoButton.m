//
//  GTClientPhotoButton.m
//  GreenTime
//
//  Created by Eduardo Dias on 31/08/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import "GTClientPhotoButton.h"

@implementation GTClientPhotoButton

- (void)awakeFromNib
{
	[super awakeFromNib];
	self.titleLabel.font = [UIFont fontWithName:kGTFont size:self.titleLabel.font.pointSize];
	
	self.backgroundColor = [UIColor gtGreen];
	self.layer.cornerRadius = 40;
	self.layer.masksToBounds = true;
}

@end
