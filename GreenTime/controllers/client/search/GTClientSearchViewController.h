//
//  GTClientSearchViewController.h
//  GreenTime
//
//  Created by Eduardo Dias on 11/10/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "Client.h"
#import "GTPlaceHolderView.h"

@interface GTClientSearchViewController : PFQueryTableViewController <UIAlertViewDelegate, UISearchBarDelegate, UISearchDisplayDelegate>

@property(strong, nonatomic) UISearchDisplayController * searchDisplay;

- (IBAction)displaySearchBar:(id)sender;

@end
