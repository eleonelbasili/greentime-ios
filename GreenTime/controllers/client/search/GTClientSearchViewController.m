//
//  GTClientSearchViewController.m
//
//  GreenTime
//
//  Created by Eduardo Dias on 30/08/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import <Parse/Parse.h>
#import "GTClientSearchViewController.h"
#import "GTClientAddViewController.h"
#import "GTClientServices.h"
#import "ClientModel.h"
#import "Client.h"
#import "GTPlaceHolderView.h"

@interface GTClientSearchViewController ()
@end

@implementation GTClientSearchViewController
{
	NSArray * _searchResults;
	NSTimer * _searchTimer;
	Client * _clientToDelete;
	GTPlaceHolderView * _emptyView;
}

#pragma mark - Initialization

- (id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if(self)
	{
		self.parseClassName = @"Client";
		self.pullToRefreshEnabled = YES;
	}
	
	return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	// Creating Search Bar
	UISearchBar * searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
	[searchBar sizeToFit];
	[searchBar setDelegate:self];
	[searchBar setHidden:YES];
	
	UIView * searchBarWrapper = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 0, 0)];
	[searchBarWrapper addSubview:searchBar];
	[searchBarWrapper setHidden:YES];
	
	self.searchDisplay = [[UISearchDisplayController alloc] initWithSearchBar:searchBar contentsController:self];
	self.searchDisplay.delegate = self;
	self.searchDisplay.searchResultsDelegate = self;
	self.searchDisplay.searchResultsDataSource = self;
	
	self.tableView.tableHeaderView = searchBarWrapper;
	
	_emptyView = [[GTPlaceHolderView alloc] initWithFrame:self.view.frame viewType:kGTClientPlaceHolder];
	_emptyView.hidden = true;
	[self.view addSubview:_emptyView];
	
	// Removing separator to empty rows
	UIView *view = [[UIView alloc] init];
	view.backgroundColor = [UIColor clearColor];
	[self.tableView setTableFooterView:view];
}

- (void)viewWillAppear:(BOOL)animated
{
	[self loadObjects];
}

- (void)objectsDidLoad:(NSError *)error
{
	[super objectsDidLoad:error];
	
	BOOL isDataEmpty = (self.objects.count > 0);
	_emptyView.hidden             = isDataEmpty;
	self.tableView.scrollEnabled = isDataEmpty;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Table View Operations

- (PFQuery *)queryForTable
{
	PFQuery * query = [PFQuery queryWithClassName:@"Client"];
	[query whereKey:@"user" equalTo:[PFUser currentUser]];
	if(self.objects.count == 0)
	{
		query.cachePolicy = kPFCachePolicyCacheThenNetwork;
	}
	[query orderByAscending:@"name"];
	return query;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return (tableView == self.tableView) ? [self.objects count] : [_searchResults count];
}

-(PFTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath object:(PFObject *)object
{
	static NSString * cellID = @"ClientCell";
	PFTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID];
	PFObject * cellData = (tableView == self.tableView) ? object : [_searchResults objectAtIndex:indexPath.row];
	if (!cell)
	{
		cell = [[PFTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
	}
	cell.textLabel.text = [cellData objectForKey:@"name"];
	cell.textLabel.font = [UIFont fontWithName:kGTFont size:kGTFontSizeMedium];
	cell.textLabel.textColor = [UIColor gtDarkGrey];
	
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.searchDisplayController.searchResultsTableView)
	{
        [self performSegueWithIdentifier: @"ShowClient" sender: self];
    }
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
	return true;
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (editingStyle == UITableViewCellEditingStyleDelete)
	{
		_clientToDelete = [self.objects objectAtIndex:indexPath.row];
		if (_clientToDelete && ![[ClientModel instance]isClientInRecordingMode:_clientToDelete])
		{
			[[[UIAlertView alloc]
			  initWithTitle:NSLocalizedString(@"client.alertViewDelete.title", nil)
			  message:NSLocalizedString(@"client.alertViewDelete.message", nil)
			  delegate:self
			  cancelButtonTitle:NSLocalizedString(@"cancel", nil)
			  otherButtonTitles:NSLocalizedString(@"ok", nil), nil] show];
		}
		else
		{
			[[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"client.recordingMode.alertView.title", nil)
										message:NSLocalizedString(@"client.recordingMode.alertView.Message", nil)
									   delegate:self
							  cancelButtonTitle:NSLocalizedString(@"ok", nil)
							  otherButtonTitles:nil, nil] show];
		}
	}
}


#pragma mark - Alert View Operations

- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex
{
	if (buttonIndex == 1)
	{
		[[GTClientServices instance] deleteClient:_clientToDelete.objectId
											block:^(Client * client, NSError *error)
		{
			if (!error)
			{
				[self loadObjects];
			}
			else
			{
				NSLog(@"Error: %@ %@", error, [error userInfo]);
			}
			_clientToDelete = nil;
		}];
	}
}


#pragma mark - Segue Operations

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"ShowClient"])
	{
        GTClientAddViewController * addClientViewController = segue.destinationViewController;
        
        NSIndexPath *indexPath;
        if ([self.searchDisplayController isActive])
		{
            indexPath = [self.searchDisplayController.searchResultsTableView indexPathForSelectedRow];
            addClientViewController.client = [_searchResults objectAtIndex:indexPath.row];
        }
		else
		{
            indexPath = [self.tableView indexPathForSelectedRow];
            addClientViewController.client = [self.objects objectAtIndex:indexPath.row];
        }
    }
}


#pragma mark - Search Operations

- (void)filterResults:(NSTimer *)timer
{
	NSString * searchText = timer.userInfo;
	
	PFQuery * upperCaseQuery = [PFQuery queryWithClassName:self.parseClassName];
	[upperCaseQuery whereKey:@"name" containsString:[searchText capitalizedString]];
	
	PFQuery * lowerCaseQuery = [PFQuery queryWithClassName:self.parseClassName];
	[lowerCaseQuery whereKey:@"name" containsString:[searchText lowercaseString]];
	
	PFQuery * regularQuery = [PFQuery queryWithClassName:self.parseClassName];
	[regularQuery whereKey:@"name" containsString:searchText];
	
	PFQuery * resultQuery = [PFQuery orQueryWithSubqueries:[NSArray arrayWithObjects:upperCaseQuery, lowerCaseQuery, regularQuery, nil]];
	
	[_searchTimer invalidate];
	_searchTimer = nil;
	
	dispatch_queue_t filterQueue = dispatch_queue_create("com.greentime.filterResults", NULL);
	dispatch_async(filterQueue, ^{
		[resultQuery findObjectsInBackgroundWithBlock:^(NSArray * objects, NSError *error)
		{
			_searchResults = objects;
			dispatch_async(dispatch_get_main_queue(), ^{
				[self.searchDisplay.searchResultsTableView reloadData];
			});
		}];
	});
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller
shouldReloadTableForSearchString:(NSString *)searchString
{
	NSString * stringTrimmed = [searchString stringByTrimmingCharactersInSet:
									  [NSCharacterSet whitespaceCharacterSet]];
	if ([stringTrimmed isEqualToString:@""])
	{
		return NO;
	}
	else
	{
		if(_searchTimer)
		{
			[_searchTimer invalidate];
			_searchTimer = nil;
		}
		_searchTimer = [NSTimer scheduledTimerWithTimeInterval:0.5
													   target:self
													 selector:@selector(filterResults:)
													 userInfo:stringTrimmed
													  repeats:NO];
		return YES;
	}
}

- (void)searchDisplayControllerWillEndSearch:(UISearchDisplayController *)controller
{
	[self.searchDisplay.searchBar resignFirstResponder];
	self.searchDisplay.searchBar.superview.frame = CGRectMake(0, 0, 0, 0);
	self.searchDisplay.searchBar.hidden = YES;
}

- (IBAction)displaySearchBar:(id)sender
{
	self.searchDisplay.searchBar.hidden = NO;
	self.searchDisplay.searchBar.superview.frame = CGRectMake(0, 0, 320, 44);
	[self.searchDisplay setActive:YES animated:YES];
}
@end
