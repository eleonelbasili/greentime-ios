//
//  GTProjectViewController.h
//  GreenTime
//
//  Created by Eduardo Dias on 17/09/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Client.h"
#import "Project.h"
#import "GTTextField.h"
#import "GTTextView.h"
#import "GTToolBarUtil.h"
#import "GTFormTableViewController.h"

@interface GTProjectViewController : GTFormTableViewController

@property(nonatomic, strong) Project * project;
@property(nonatomic, weak) Client * client;

@property (strong, nonatomic) IBOutlet GTTextField * projectTextField;
@property (strong, nonatomic) IBOutlet GTTextView * notesTextView;

- (IBAction)done:(id)sender;

@end
