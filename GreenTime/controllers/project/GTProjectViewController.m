//
//  GTProjectViewController.m
//  GreenTime
//
//  Created by Eduardo Dias on 17/09/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import "GTProjectViewController.h"
#import "MBProgressHUD.h"

@interface GTProjectViewController ()

@end

@implementation GTProjectViewController

#pragma - Initialization

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	if (self.project == nil)
	{
		self.project = [[Project alloc] initWithClient:self.client];
		self.title = NSLocalizedString(@"project.title", nil);
		self.navigationItem.rightBarButtonItem.title = NSLocalizedString(@"project.button.add", nil);
	}
	else
	{
		self.title = @"";
		self.navigationItem.rightBarButtonItem.title = NSLocalizedString(@"project.button.update", nil);
	}
	
	self.navigationItem.rightBarButtonItem.enabled = self.project.name.length > 0;
	
	[self.projectTextField setInputAccessoryView:self.toolbar];
	self.projectTextField.nextTextInput = self.notesTextView;
	self.projectTextField.text = self.project.name;
	
	self.notesTextView.text = self.project.notes;
	[self.notesTextView setPlaceHolderText:NSLocalizedString(@"project.notes", nil)];
	[self.notesTextView setInputAccessoryView:self.toolbar];
	self.notesTextView.previousTextInput = self.projectTextField;
	
}

- (void)viewDidAppear:(BOOL)animated
{
	[self.projectTextField becomeFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma - TextInput Operations

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
	NSString * newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
	NSString * trimString = [newString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
	self.navigationItem.rightBarButtonItem.enabled = (trimString.length > 0);
	
	return true;
}

- (IBAction)done:(id)sender
{
	self.project.name = self.projectTextField.text;
	self.project.notes = self.notesTextView.text;
	if (!self.project.objectId)
	{
		[self.client addProject:self.project];
	}
	[self.navigationController popViewControllerAnimated:YES];
}

@end
