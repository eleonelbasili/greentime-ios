//
//  GTPlayButtonView.m
//  GreenTime
//
//  Created by Eduardo Dias on 7/11/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import "GTPlayButtonBackground.h"

@implementation GTPlayButtonBackground

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
	CGContextSetStrokeColorWithColor(context, [UIColor gtGrey].CGColor);
	
	CGContextSetLineWidth(context, 0.5);
	CGContextMoveToPoint(context, 0.0, 0.0);
	CGContextAddLineToPoint(context, rect.size.width, 0.0);
	
	CGContextStrokePath(context);
}

@end
