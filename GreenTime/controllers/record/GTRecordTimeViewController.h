//
//  GTRecording.h
//  GreenTime
//
//  Created by Eduardo Dias on 21/10/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GTFormTableViewController.h"
#import "GTTextField.h"
#import "Client.h"
#import "Project.h"
#import "Timesheet.h"
#import "GTLocationManagerDelegate.h"
#import "GTLocationManager.h"
#import "GTButton.h"

@interface GTRecordTimeViewController : GTFormTableViewController <UIPickerViewDataSource, UIPickerViewDelegate, GTLocationManagerDelegate, UIAlertViewDelegate, GTButtonDelegate>

@property (strong, nonatomic) NSArray * clients;
@property (strong, nonatomic) NSArray * projects;
@property (nonatomic) BOOL isTimerOn;

@property (strong, nonatomic) NSString * regionID;
@property (strong, nonatomic) Client  * client;
@property (strong, nonatomic) Project * project;
@property (strong, nonatomic) UIPickerView * clientPicker;
@property (strong, nonatomic) UIPickerView * projectPicker;

@property (strong, nonatomic) IBOutlet UIBarButtonItem * locationButton;
@property (strong, nonatomic) IBOutlet GTTextField     * clientTextField;
@property (strong, nonatomic) IBOutlet GTTextField     * projectTextField;
@property (strong, nonatomic) IBOutlet GTFontLabel     * autoStartLabel;
@property (strong, nonatomic) IBOutlet UISwitch        * locationSwitch;
@property (strong, nonatomic) IBOutlet UILabel         * switchLabel;
@property (strong, nonatomic) IBOutlet UITableViewCell * timerCell;
@property (strong, nonatomic) IBOutlet UILabel         * timerDisplay;
@property (strong, nonatomic) IBOutlet GTButton        * timerButton;

- (IBAction)locationButtonAction:(id)sender;
- (IBAction)autoStartSwitchAction:(id)sender;

@end
