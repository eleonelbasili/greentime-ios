//
//  GTRecording.m
//  GreenTime
//
//  Created by Eduardo Dias on 21/10/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import <Parse/Parse.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "GTRecordTimeViewController.h"
#import "GTTimesheetServices.h"
#import "GTLocationManager.h"
#import "GTClientServices.h"
#import "GTProjectServices.h"
#import "GTFontLabel.h"

// Models
#import "ClientModel.h"
#import "Client.h"

// Timer Constants
NSString * const kGTTimeDisplay = @"00:00:00";

@interface GTRecordTimeViewController ()

@end

@implementation GTRecordTimeViewController
{
	NSTimer         * _timer;
	NSDateFormatter * _timeFormatter;
	
	Timesheet * _timesheet;
	
	int  _nextInitStep;
	BOOL _didEnterRegion;
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if (self)
	{
		[GTLocationManager instance].delegate = self;
		
		_timeFormatter = [[NSDateFormatter alloc] init];
		[_timeFormatter setDateFormat:@"HH:mm:ss"];
		[_timeFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0.0]];
		
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(didStartMonitoringRegion:)
													 name:kGTDidStartMonitoringRegion
												   object:nil];
		
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(didStopMonitoringRegion)
													 name:kGTDidStopMonitoringRegion
												   object:nil];
		
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(didEnterRegion:)
													 name:kGTDidEnterRegion
												   object:nil];
		
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(didExitRegion:)
													 name:kGTDidExitRegion
												   object:nil];
	}
	return self;
}

#pragma mark - Initialization

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	// Localizing texfields
	self.autoStartLabel.text          = NSLocalizedString(@"recording.autoStartLabel", nil);
	self.clientTextField.placeholder  = NSLocalizedString(@"recording.clients", nil);
	self.projectTextField.placeholder = NSLocalizedString(@"recording.projects", nil);
	
	// Adding picker
	self.clientPicker = [self uiPickerView];
	[self.view addSubview:self.clientPicker];
	
	self.clientTextField.inputView = self.clientPicker;
	[self setupTextFieldNavigation:self.clientTextField forward:self.projectTextField backward:nil];
	
	self.projectPicker = [self uiPickerView];
	[self.view addSubview:self.projectPicker];
	
	// Setting navigation
	self.projectTextField.inputView = self.projectPicker;
	[self setupTextFieldNavigation:self.projectTextField forward:nil backward:self.clientTextField];
	
	// Setting switch
	[self.locationSwitch setOnTintColor:[UIColor gtGreen]];
	[self.locationSwitch setTintColor:[UIColor gtGreen]];
	
	// Setting timer button
	self.timerButton.delegate = self;
	self.timerButton.enabled = false;
	
	
	[[RACObserve(self, clients) skip:1] subscribeNext:^(NSArray * clients)
	 {
		 [self updateClientSelection];
	 }];
	
	[[RACObserve(self, client) skip:1] subscribeNext:^(id client)
	 {
		 [self enableTimerAndSwitchButtons:false];
		 [self findAllProjects];
	 }];
	
	[[RACObserve(self, projects) skip:1] subscribeNext:^(NSArray * projects)
	 {
		 [self updateProjectSelection];
	 }];
	
	[[RACObserve(self, project) skip:1] subscribeNext:^(Project * project)
	 {
		 [self enableTimerAndSwitchButtons:project != nil];
		 [self updateAvailableLocationsLabel];
		 [self autoStartByLocation];
	 }];
	
	[[RACObserve(self, isTimerOn) skip:1] subscribeNext:^(id isOn)
	 {
		 [self enableControls:![isOn boolValue]];
		 [self resetTimerDisplay];
		 [[ClientModel instance] setClientInRecordingMode: [isOn boolValue] ? self.client : nil];
	 }];
	
}

-(void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
	
	// Adjusting Timer Display to fit iPhone 3.5 screen
	CGFloat timerCellHeight = self.tableView.frame.size.height - 132.0;
	CGFloat timerDisplayAreaHeight = timerCellHeight - 157;
	self.timerCell.frame =  CGRectMake(0, 132, 320, timerCellHeight);
	self.timerDisplay.frame = CGRectMake(7, timerDisplayAreaHeight / 2 - 42, 307, 84);
	
	if (!self.isTimerOn)
	{
		[self refreshView];
	}
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - Initialization Operations

-(void)refreshView
{
	self.regionID = nil;
	[self refreshRegionID];
	[self findClients];
}

-(void)refreshRegionID
{
	[[GTLocationManager instance]findCurrentLocationInMonitoredRegions:^(CLCircularRegion * region)
	 {
		 self.regionID = (region)?region.identifier:nil;
	 }];
}

-(void)findClients
{
	[[GTClientServices instance] findClientsWithBlock:^(NSArray * clients, NSError *error)
	 {
		 if (!error)
		 {
			 self.clients = clients;
			 [self.clientPicker reloadAllComponents];
		 }
	 }];
}

-(void)updateClientSelection
{
	if (self.clients.count > 0)
	{
		NSArray * clientFound;
		if (self.regionID)
		{
			NSString * clientName = [self clientNameFromRegionId:self.regionID];
			clientFound = [self.clients filteredArrayUsingPredicate:
						   [NSPredicate predicateWithFormat:@"self.name = %@", clientName]];
		}
		else
		{
			clientFound = [self.clients filteredArrayUsingPredicate:
						   [NSPredicate predicateWithFormat:@"self.objectId = %@.objectId", self.client]];
		}
		
		self.client = (clientFound.count > 0) ? [clientFound objectAtIndex:0] : [self.clients objectAtIndex:0];
		self.clientTextField.text = self.client.name;
		
		NSInteger clientPickerIndex = [self.clients indexOfObject:self.client];
		[self.clientPicker selectRow:clientPickerIndex inComponent:0 animated:false];
	}
	else
	{
		self.clientTextField.text = @"";
	}
}

-(void)findAllProjects
{
	[[GTProjectServices instance] findProjectsWithClientId:self.client.objectId
												  block:^(NSArray * projects, NSError *error)
	 {
		 if (!error)
		 {
			 self.projects = projects;
			 [self.projectPicker reloadAllComponents];
		 }
	 }];
}

-(void)updateProjectSelection
{
	if (self.projects.count > 0)
	{
		NSArray * projectFound;
		if (self.regionID)
		{
			NSString * projectName = [self projectNameFromRegionId:self.regionID];
			projectFound = [self.projects filteredArrayUsingPredicate:
							[NSPredicate predicateWithFormat:@"self.name = %@", projectName]];
		}
		else
		{
			projectFound = [self.projects filteredArrayUsingPredicate:
							[NSPredicate predicateWithFormat:@"self.objectId = %@.objectId", self.project]];
		}
		
		self.project = (projectFound.count > 0) ? [projectFound objectAtIndex:0] : [self.projects objectAtIndex:0];
		self.projectTextField.text = self.project.name;
		
		NSInteger projectPickerIndex = [self.projects indexOfObject:self.project];
		[self.projectPicker selectRow:projectPickerIndex inComponent:0 animated:false];
	}
	else
	{
		self.projectTextField.text = @"";
	}
}


#pragma mark - Picker View Operations

-(UIPickerView *)uiPickerView
{
	CGRect pickerFrame = CGRectMake(0, self.view.bounds.size.height, self.view.bounds.size.width, 162);
	UIPickerView * uiPickerView = [[UIPickerView alloc] initWithFrame:pickerFrame];
	uiPickerView.dataSource = self;
	uiPickerView.showsSelectionIndicator = NO;
	uiPickerView.delegate = self;
	uiPickerView.backgroundColor = [UIColor gtGrey];
	
	UIView * selector = [[UIView alloc] initWithFrame:CGRectMake(0, 58, self.view.bounds.size.width, 46)];
	selector.backgroundColor = [UIColor gtGreen];
	selector.alpha = 0.4;
	[uiPickerView addSubview:selector];
	
	return uiPickerView;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
	return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
	return (pickerView == self.clientPicker) ? self.clients.count : self.projects.count;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
	return 44;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
	GTFontLabel * viewLabel = (GTFontLabel *)view;
	if (viewLabel == nil)
	{
		viewLabel = [[GTFontLabel alloc]
					 initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 100)
					 font:[UIFont fontWithName:kGTFont size:kGTFontSizeLarge]];
		[viewLabel setTextAlignment:NSTextAlignmentCenter];
		
	}
	
	if ((pickerView == self.clientPicker) && (self.clients.count > 0))
	{
		Client * client = [self.clients objectAtIndex:row];
		viewLabel.text = client.name;
	}
	else if (self.projects.count > row)
	{
		Project * project = [self.projects objectAtIndex:row];
		viewLabel.text = project.name;
	}
	
	return viewLabel;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
	if (pickerView == self.clientPicker)
	{
		self.client = [self.clients objectAtIndex:row];
		self.clientTextField.text = self.client.name;
	}
	else if (self.projects.count > 0)
	{
		self.project = [self.projects objectAtIndex:row];
		self.projectTextField.text = self.project.name;
		
		[self updateAvailableLocationsLabel];
	}
}


#pragma mark - Enable Operations

-(void)enableTextFields:(bool)enable
{
	self.clientTextField.enabled  = enable;
	self.projectTextField.enabled = enable;
}

-(void)enableLocationSwitch:(bool)enable
{
	self.locationSwitch.enabled   = enable;
	self.locationSwitch.tintColor = (enable) ? [UIColor gtGreen] : [UIColor gtGrey];
}

-(void)enableControls:(bool)enable
{
	[self enableTextFields:enable];
	self.locationButton.enabled = enable;
	[self enableLocationSwitch:enable && [[GTLocationManager instance] availableLocations] > 0];
}

-(void)enableTimerAndSwitchButtons:(bool)enable
{
	self.timerButton.enabled    = enable;
	[self enableLocationSwitch:enable && [[GTLocationManager instance] availableLocations] > 0];
}


#pragma mark - Default Switch Operations

-(void)updateAvailableLocationsLabel
{
	CLRegion * monitoredRegion = [[GTLocationManager instance] findMonitoredRegionWithKey:[self createRegionId]];
	self.locationSwitch.on = (monitoredRegion != nil);
	self.autoStartLabel.highlighted = (monitoredRegion != nil);
	
	int availableLocations = [[GTLocationManager instance] availableLocations];
	NSString * locationsAvailableText = [NSString stringWithFormat:@"%d of %d",
										 availableLocations, kGTMaxLocationsAvailable];
	
	NSMutableAttributedString * labelAttributes = [[NSMutableAttributedString alloc] initWithString:locationsAvailableText];
	[labelAttributes addAttribute:NSForegroundColorAttributeName
							value:[UIColor gtGreen]
							range:NSMakeRange(0, 2)];
	[labelAttributes addAttribute:NSForegroundColorAttributeName
							value:[UIColor gtGreen]
							range:NSMakeRange(6, 2)];
	
	[self.switchLabel setAttributedText:labelAttributes];
}


#pragma mark - Timer button operations

-(void)buttonDidEnterOnPlayState
{
	self.isTimerOn = true;
	
	_timesheet = [[Timesheet alloc] initWithProject:self.project];
	_timesheet.startTime = [NSDate date];
	_timer = [NSTimer scheduledTimerWithTimeInterval:1.0
											 target:self
										   selector:@selector(updateTimerDisplay)
										   userInfo:nil repeats:YES];
}

-(void)buttonDidEnterOnStopState
{
	self.isTimerOn = false;
	
	[_timer invalidate];
	_timesheet.endTime = [NSDate date];
	[[GTTimesheetServices instance]saveTimesheet:[_timesheet toDictionary]
										   block:^(Timesheet * timesheet, NSError *error)
	 {
		 if (!error)
		 {
			 [self refreshView];
		 }
	 }];
}


#pragma mark - Timer Reset Operations

-(void)resetTimerDisplay
{
	self.timerDisplay.text = kGTTimeDisplay;
	self.timerDisplay.highlighted = self.isTimerOn;
}


#pragma mark - Timer Operations

-(void)updateTimerDisplay
{
    self.timerDisplay.text = [NSDate timeIntervalWithDates:_timesheet.startTime endDate:[NSDate date]];
}


#pragma mark - Location Operations

-(void)autoStartByLocation
{
	if (self.regionID && _didEnterRegion)
	{
		_didEnterRegion = false;
		self.regionID = nil;
		self.timerButton.enabled = true;
		[self.timerButton sendActionsForControlEvents:UIControlEventTouchUpInside];
	}
}

-(void)locationButtonAction:(id)sender
{
	[self refreshView];
}

-(NSString *)createRegionId
{
	NSString * regionId;
	
	if (self.client && self.project)
	{
		regionId = [NSString stringWithFormat:@"%@%@%@", self.client.name, @"_", self.project.name];
	}
	return regionId;
}

-(NSString *)clientNameFromRegionId:(NSString *)regionId
{
	NSArray * region = [regionId componentsSeparatedByString:@"_"];
	return [region objectAtIndex:0];
}

-(NSString *)projectNameFromRegionId:(NSString *)regionId
{
	NSArray * region = [regionId componentsSeparatedByString:@"_"];
	return [region objectAtIndex:1];
}

- (IBAction)autoStartSwitchAction:(id)sender
{
	if (self.locationSwitch.isOn)
	{
		[[GTLocationManager instance]startMonitoringCurrentLocationWithKey:[self createRegionId]];
	}
	else
	{
		[[GTLocationManager instance] stopMonitoringCurrentLocationWithKey:[self createRegionId]];
	}
}

-(void)currentLocationWasMonitored:(CLCircularRegion *)region
{
	NSString * regionId = region.identifier;
	NSString * client = [self clientNameFromRegionId:regionId];
	NSString * project = [self projectNameFromRegionId:regionId];
	NSString * message = [NSString stringWithFormat:@"%@ %@ %@ %@. %@ %@ %@ %@?",
						  NSLocalizedString(@"recording.regionMonitored.firstSentence", nil), client, @"-", project,
						  NSLocalizedString(@"recording.regionMonitored.secondSentence", nil), self.client, @"-", self.project];
	
	[[[UIAlertView alloc] initWithTitle:@"Location Service"
								message:message
							   delegate:self
					  cancelButtonTitle:NSLocalizedString(@"No", nil)
					  otherButtonTitles:NSLocalizedString(@"Yes", nil), nil]show];
}

-(void)locationKeyWasNotAvailable:(NSString *)locationKey
{
	[[[UIAlertView alloc] initWithTitle:@"Location Service"
							    message:NSLocalizedString(@"recording.settingInUse", nil)
							   delegate:self
					  cancelButtonTitle:NSLocalizedString(@"No", nil)
					  otherButtonTitles:NSLocalizedString(@"Yes", nil), nil]show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if(buttonIndex == 1)
	{
		[[GTLocationManager instance] updateMonitoringSteps];
	}
	else
	{
		[[GTLocationManager instance] stopMonitoringSteps];
		self.locationSwitch.on = false;
	}
}


#pragma mark - Location Notifications

-(void)didStartMonitoringRegion:(NSNotification *)notification
{
	[self updateAvailableLocationsLabel];
	if (_timer == nil || !_timer.isValid)
	{
		[self.timerButton sendActionsForControlEvents:UIControlEventTouchUpInside];
	}
}

-(void)didStopMonitoringRegion
{
	self.regionID = nil;
	[self updateAvailableLocationsLabel];
}

-(void)didEnterRegion:(NSNotification *)notification
{
	_didEnterRegion = true;
	if (self.isTimerOn)
	{
		[self.timerButton sendActionsForControlEvents:UIControlEventTouchUpInside];
	}
	else
	{
		[self refreshView];
	}
}

-(void)didExitRegion:(NSNotification *)notification
{
	if(self.isTimerOn)
	{
		[self.timerButton sendActionsForControlEvents:UIControlEventTouchUpInside];
	}
}

@end
