//
//  GTTimesheetAddViewController.h
//  GreenTime
//
//  Created by Eduardo Dias on 22/11/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import "GTFormTableViewController.h"
#import "GTBarButtomItem.h"
#import "Client.h"
#import "Project.h"
#import "GTCalendar.h"

@interface GTTimesheetAddViewController : GTFormTableViewController <UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate, GTCalendarDelegate>

@property (strong, nonatomic) IBOutlet GTBarButtomItem * saveButton;
@property (strong, nonatomic) IBOutlet GTTextField * clientTextfield;
@property (strong, nonatomic) IBOutlet GTTextField * projectTextfield;
@property (strong, nonatomic) IBOutlet GTTextField * dateRangeTextfield;
@property (strong, nonatomic) IBOutlet GTTextField * startingTimeTextfield;
@property (strong, nonatomic) IBOutlet GTTextField * endingTimeTextfield;

@property (strong, nonatomic) UIPickerView * clientPicker;
@property (strong, nonatomic) UIPickerView * projectPicker;
@property (strong, nonatomic) UIPickerView * startTimePicker;
@property (strong, nonatomic) UIPickerView * endTimePicker;

@property (strong, nonatomic) Client * client;
@property (strong, nonatomic) Project * project;

- (IBAction)saveTimesheet:(id)sender;

@end
