//
//  GTTimesheetAddViewController.m
//  GreenTime
//
//  Created by Eduardo Dias on 22/11/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import "GTTimesheetAddViewController.h"
#import "GTClientServices.h"
#import "GTTimesheetServices.h"
#import "GTProjectServices.h"

@interface GTTimesheetAddViewController ()

@end

#pragma mark - Initialization

@implementation GTTimesheetAddViewController
{
	NSMutableArray * _hours;
	NSMutableArray * _minutes;
	NSArray * _periods;
	
	NSDate * _calendarStartDate;
	NSDate * _calendarEndDate;
	
	NSArray * _clients;
	NSArray * _projects;
	
	GTCalendar * _calendar;
	UITextField * _textfieldInEdition;
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if (self)
	{
		_hours = [[NSMutableArray alloc] init];
		for (int i = 0; i <= 12; i++)
		{
			NSString * hour;
			if (i < 10)
			{
				hour = [NSString stringWithFormat:@"%@%d", @"0", i];
			}
			else
			{
				hour = [NSString stringWithFormat:@"%d", i];
			}
			[_hours addObject:hour];
		}
		
		_minutes = [[NSMutableArray alloc] init];
		for (int i = 0; i < 60; i++)
		{
			NSString * minute;
			if (i < 10)
			{
				minute = [NSString stringWithFormat:@"%@%d", @"0", i];
			}
			else
			{
				minute = [NSString stringWithFormat:@"%d", i];
			}
			[_minutes addObject:minute];
		}
		
		_periods = @[@"AM", @"PM"];
	}
	return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	
	self.clientPicker = [self uiPickerView];
	self.clientTextfield.inputView = self.clientPicker;
	[self.view addSubview:self.clientPicker];
	[self setupTextFieldNavigation:self.clientTextfield
						   forward:self.projectTextfield backward:nil];
	
	self.projectPicker = [self uiPickerView];
	self.projectTextfield.inputView = self.projectPicker;
	[self.view addSubview:self.projectPicker];
	[self setupTextFieldNavigation:self.projectTextfield
						   forward:self.dateRangeTextfield backward:self.clientTextfield];
	
	self.dateRangeTextfield.inputView = _calendar;
	[self setupTextFieldNavigation:self.dateRangeTextfield
						   forward:self.startingTimeTextfield backward:self.projectTextfield];
	
	self.startTimePicker = [self uiPickerView];
	self.startingTimeTextfield.inputView = self.startTimePicker;
	[self.startingTimeTextfield setBorders:BorderRight | BorderBottom];
	[self.view addSubview:self.startTimePicker];
	[self setupTextFieldNavigation:self.startingTimeTextfield
							forward:self.endingTimeTextfield backward:self.dateRangeTextfield];
	
	self.endTimePicker = [self uiPickerView];
	[self.endingTimeTextfield setContentInsets:UIEdgeInsetsMake(0, 10, 0, 0)];
	self.endingTimeTextfield.inputView = self.endTimePicker;
	[self.endingTimeTextfield setBorders:BorderBottom];
	[self.view addSubview:self.endTimePicker];
	[self setupTextFieldNavigation:self.endingTimeTextfield
						   forward:nil backward:self.startingTimeTextfield];
	
	self.saveButton.enabled = false;
}

-(void)viewWillAppear:(BOOL)animated
{
	[self loadClients];
}

- (void)viewDidAppear:(BOOL)animated
{
	if (!_calendar.isVisible)
	{
		[self.clientTextfield becomeFirstResponder];
	}
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - Load Operations

-(void)loadClients
{
	[[GTClientServices instance] findClientsWithBlock:^(NSArray * remoteClients, NSError *error)
	{
		if (!error)
		{
			_clients = (remoteClients) ? remoteClients : [[NSArray alloc] init];
			[self.clientPicker reloadAllComponents];
			[self updateClientSelection];
		}
	}];
}

-(void)updateClientSelection
{
	if (_clients.count > 0)
	{
		self.client = [_clients objectAtIndex:0];
		self.clientTextfield.text = self.client.name;
		[self.clientPicker selectRow:0 inComponent:0 animated:false];
		
		[self loadProjects];
	}
	else
	{
		self.clientTextfield.text = @"";
	}
}

-(void)loadProjects
{
	[[GTProjectServices instance] findProjectsWithClientId:self.client.objectId
												  block:^(NSArray * remoteProjects, NSError *error)
	{
		if (!error)
		{
			_projects = (remoteProjects) ? remoteProjects : [[NSArray alloc] init];
			[self.projectPicker reloadAllComponents];
			
			[self updateProjectSelection];
		}
	}];
}

-(void)updateProjectSelection
{
	if (_projects.count > 0)
	{
		self.project = [_projects objectAtIndex:0];
		self.projectTextfield.text = self.project.name;
		[self.projectPicker selectRow:0 inComponent:0 animated:false];
	}
	else
	{
		self.projectTextfield.text = @"";
	}
}


#pragma mark - Textfield Operations

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField == self.dateRangeTextfield)
	{
		UITextField * currentTextfield = (UITextField *)self.textInputInEdition;
		[currentTextfield resignFirstResponder];
		_textfieldInEdition = currentTextfield;
		[self toggleCalendar];
		return NO;
	}
	return YES;
}


#pragma mark - Calendar Operations

-(void)toggleCalendar
{
	if (!_calendar)
	{
		CGFloat tabBarHeight = self.tabBarController.tabBar.frame.size.height;
		CGFloat navBarHeight = self.navigationController.navigationBar.frame.size.height;
		CGFloat statusBarHeight = [[UIApplication sharedApplication] statusBarFrame].size.height;
		CGFloat calendarHeight = self.view.superview.frame.size.height - navBarHeight - statusBarHeight - tabBarHeight;
		
		CGRect initFrame = CGRectMake(0, - calendarHeight, 320, calendarHeight);
		CGRect finalFrame = CGRectMake(0, 0, 320, calendarHeight);
		
		_calendar = [[GTCalendar alloc] initWithDelegate:self initFrame:initFrame finalFrame:finalFrame];
		[_calendar setButtonTitle:@"Done"];
		
		[self.view addSubview:_calendar];
	}
	[_calendar toggle];
}

-(void)selectedDates:(NSDate *)startDate endDate:(NSDate *)endDate
{
	if (startDate && endDate)
	{
		_calendarStartDate = startDate;
		_calendarEndDate   = endDate;
		self.dateRangeTextfield.text = [NSDate dateRangeWithDates:startDate endDate:endDate];
	}
	[self updateSaveButtonStatus];
	[self toggleCalendar];
}


#pragma mark - Calendar delegates

-(void)calendarDidShow{}

-(void)calendarDidHide
{
	if (_textfieldInEdition)
	{
		if (_textfieldInEdition == self.projectTextfield)
		{
			[self.startingTimeTextfield becomeFirstResponder];
		}
		else
		{
			[self.projectTextfield becomeFirstResponder];
		}
	}
}


#pragma mark - Picker View Operations

-(UIPickerView *)uiPickerView
{
	CGRect pickerFrame = CGRectMake(0, self.view.bounds.size.height, self.view.bounds.size.width, 162);
	UIPickerView * uiPickerView = [[UIPickerView alloc] initWithFrame:pickerFrame];
	uiPickerView.dataSource = self;
	uiPickerView.delegate = self;
	uiPickerView.showsSelectionIndicator = NO;
	uiPickerView.backgroundColor = [UIColor gtGrey];
	
	UIView * selector = [[UIView alloc] initWithFrame:CGRectMake(0, 58, self.view.bounds.size.width, 46)];
	selector.backgroundColor = [UIColor gtGreen];
	selector.alpha = 0.4;
	[uiPickerView addSubview:selector];
	
	return uiPickerView;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
	return (pickerView == self.startTimePicker || pickerView == self.endTimePicker) ? 3 : 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
	NSInteger numberOfRows = 0;
	if (pickerView == self.clientPicker)
	{
		numberOfRows = _clients.count;
	}
	else if (pickerView == self.projectPicker)
	{
		numberOfRows = _projects.count;
	}
	else
	{
		switch (component)
		{
			case 0:
				numberOfRows = _hours.count;
			break;
			case 1:
				numberOfRows = _minutes.count;
			break;
			case 2:
				numberOfRows = _periods.count;
			break;
		}
	}
	return numberOfRows;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
	return 44;
}

-(CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
	return (pickerView == self.startTimePicker || pickerView == self.endTimePicker) ? 50 : self.view.bounds.size.width;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
	GTFontLabel * viewLabel = (GTFontLabel *)view;
	if (viewLabel == nil)
	{
		viewLabel = [[GTFontLabel alloc]
					 initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 100)
					 font:[UIFont fontWithName:kGTFont size:kGTFontSizeLarge]];
		[viewLabel setTextAlignment:NSTextAlignmentCenter];
	}
	
	if (pickerView == self.clientPicker)
	{
		Client * client = (Client *)[_clients objectAtIndex:row];
		
		viewLabel.text = client.name;
	}
	else if (pickerView == self.projectPicker)
	{
		Project * project = (Project *)[_projects objectAtIndex:row];
		viewLabel.text = project.name;
	}
	else
	{
		switch (component)
		{
			case 0:
				viewLabel.text = [_hours objectAtIndex:row];
			break;
			case 1:
				viewLabel.text = [_minutes objectAtIndex:row];
			break;
			case 2:
				viewLabel.text = [_periods objectAtIndex:row];
			break;
		}
	}
	return viewLabel;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
	if (pickerView == self.clientPicker)
	{
		self.client = [_clients objectAtIndex:row];
		self.clientTextfield.text = self.client.name;
		
		[self loadProjects];
	}
	else if (pickerView == self.projectPicker)
	{
		self.project = [_projects objectAtIndex:row];
		self.projectTextfield.text = self.project.name;
	}
	else
	{
		NSString * hoursStr;
		NSString * minutesStr;
		NSString * periodStr;
		switch (component)
		{
			case 0:
			{
				hoursStr = [_hours objectAtIndex:row];
				NSInteger selMinute = [pickerView selectedRowInComponent:1];
				minutesStr = [_minutes objectAtIndex:selMinute];
				NSInteger selPeriod = [pickerView selectedRowInComponent:2];
				periodStr = [_periods objectAtIndex:selPeriod];
			}
			break;
			case 1:
			{
				NSInteger selHour = [pickerView selectedRowInComponent:0];
				hoursStr = [_hours objectAtIndex:selHour];
				minutesStr = [_minutes objectAtIndex:row];
				NSInteger selPeriod = [pickerView selectedRowInComponent:2];
				periodStr = [_periods objectAtIndex:selPeriod];
			}
			break;
			case 2:
			{
				NSInteger selHour = [pickerView selectedRowInComponent:0];
				hoursStr = [_hours objectAtIndex:selHour];
				NSInteger selMinute = [pickerView selectedRowInComponent:1];
				minutesStr = [_minutes objectAtIndex:selMinute];
				periodStr = [_periods objectAtIndex:row];
			}
			break;
		}
		
		NSString * timeFormated = [NSString stringWithFormat:@"%@:%@ %@", hoursStr, minutesStr, periodStr];
		if (pickerView == self.startTimePicker)
		{
			self.startingTimeTextfield.text = timeFormated;
		}
		else
		{
			self.endingTimeTextfield.text = timeFormated;
		}
	}
	[self updateSaveButtonStatus];
}


#pragma mark - Save Button Operations

-(void)updateSaveButtonStatus
{
	self.saveButton.enabled = (![self.clientTextfield.text isEqualToString:@""] &&
							   ![self.projectTextfield.text isEqualToString: @""] &&
							   ![self.dateRangeTextfield.text isEqualToString: @""] &&
							   ![self.startingTimeTextfield.text isEqualToString: @""] &&
							   ![self.endingTimeTextfield.text isEqualToString: @""]);
}

- (IBAction)saveTimesheet:(id)sender
{
	Timesheet * timesheet = [[Timesheet alloc] initWithProject:self.project];
	NSString * startDateStr = [NSString stringWithFormat:@"%@ %@",
						[_calendarStartDate compactDate], self.startingTimeTextfield.text];
	
	NSString * endDateStr = [NSString stringWithFormat:@"%@ %@",
							   [_calendarEndDate compactDate], self.endingTimeTextfield.text];
	
	NSDateFormatter * formatter = [[NSDateFormatter alloc]init];
	[formatter setDateFormat:@"dd/MM/yyyy hh:mm a"];
	
	NSDate * startDate = [formatter dateFromString:startDateStr];
	NSDate * endDate = [formatter dateFromString:endDateStr];
	
	timesheet.startTime = startDate;
	timesheet.endTime   = endDate;
	
	[self showHUD:MBProgressHUDModeIndeterminate label:NSLocalizedString(@"saving", nil)];
	[[GTTimesheetServices instance] saveTimesheet:[timesheet toDictionary]
											block:^(Timesheet * timesheet, NSError * error)
	{
		if (!error)
		{
			[self.navigationController popViewControllerAnimated:YES];
		}
		[self hideHUD];
	}];
}
@end
