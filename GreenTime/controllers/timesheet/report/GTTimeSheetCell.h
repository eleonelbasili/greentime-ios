//
//  GTProjectTimeSheetCell.h
//  GreenTime
//
//  Created by Eduardo Dias on 1/11/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GTFontLabel.h"

@interface GTTimeSheetCell : UITableViewCell

@property (strong, nonatomic) IBOutlet GTFontLabel * dateLabel;
@property (strong, nonatomic) IBOutlet GTFontLabel * startTimeLabel;
@property (strong, nonatomic) IBOutlet GTFontLabel * endTimeLabel;
@property (strong, nonatomic) IBOutlet GTFontLabel * timeIntervalLabel;

@end
