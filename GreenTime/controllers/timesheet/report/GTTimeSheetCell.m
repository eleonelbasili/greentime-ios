//
//  GTProjectTimeSheetCell.m
//  GreenTime
//
//  Created by Eduardo Dias on 1/11/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import "GTTimeSheetCell.h"

@implementation GTTimeSheetCell

-(id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if (self)
	{
		self.dateLabel.textColor = [UIColor blackColor];
		self.startTimeLabel.textColor = [UIColor gtGreen];
		self.endTimeLabel.textColor = [UIColor gtRed];
		self.timeIntervalLabel.textColor = [UIColor blackColor];
	}
	return self;
}

-(void)layoutSubviews
{
	[super layoutSubviews];
	self.dateLabel.font = [UIFont fontWithName:kGTFontBold size:kGTFontSizeSmall];
}

@end
