//
//  GTProjectHoursViewController.h
//  GreenTime
//
//  Created by Eduardo Dias on 31/10/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GTBaseViewController.h"
#import "ClientTimesheet.h"
#import "Timesheet.h"

@interface GTTimesheetClientViewController : GTBaseViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UIBarButtonItem * totalTimeLabel;
@property (strong, nonatomic) IBOutlet UITableView * tableView;
@property (strong, nonatomic) ClientTimesheet * clientTimesheet;

@property (strong, nonatomic) NSDate * startDate;
@property (strong, nonatomic) NSDate * endDate;

@end
