//
//  GTProjectHoursViewController.m
//  GreenTime
//
//  Created by Eduardo Dias on 31/10/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import "GTTimesheetClientViewController.h"
#import "GTTimesheetServices.h"
#import "GTTimeSheetCell.h"
#import "Timesheet.h"
#import "ProjectTimesheet.h"
#import "GTFontLabel.h"

@interface GTTimesheetClientViewController ()
{
	NSMutableArray * _projectSectionTotalTimeLabels;
}
@end

@implementation GTTimesheetClientViewController
{
	Timesheet * _timesheetToDelete;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	_projectSectionTotalTimeLabels = [[NSMutableArray alloc] init];
	
	self.title = self.clientTimesheet.client.name;
	[self.totalTimeLabel
	 setTitleTextAttributes:@{NSFontAttributeName: [UIFont fontWithName:kGTFont size:kGTFontSizeSmall],
							  NSForegroundColorAttributeName: [UIColor gtWhite]
							  } forState:UIControlStateNormal];
	
	[self loadTimesheetsAndCalculateTotalTime];
	
	// Removing separator to empty rows
	UIView *view = [[UIView alloc] init];
	view.backgroundColor = [UIColor clearColor];
	[self.tableView setTableFooterView:view];
}

-(void)loadTimesheetsAndCalculateTotalTime
{
	[self showHUD:MBProgressHUDModeIndeterminate label:NSLocalizedString(@"Loading", nil)];
	[[GTTimesheetServices instance] projectsTimesheetInDateRange:self.clientTimesheet.client.objectId
													   startDate:self.startDate
														 endDate:self.endDate
														   block:^(NSArray * projectsTimesheetDict, NSError *error)
	 {
		 if (!error)
		 {
			 self.clientTimesheet.projects = [[NSMutableArray alloc] init];
			 for (NSDictionary * projectTimesheetDict in projectsTimesheetDict)
			 {
				 ProjectTimesheet * projectTimesheet = [[ProjectTimesheet alloc]
														initWithDictionary:projectTimesheetDict];
				 [self.clientTimesheet.projects addObject:projectTimesheet];
			 }
			 [self calculateProjectsTotalTime];
			 [self.tableView reloadData];
		 }
		 [self hideHUD];
	 }];
}

-(void)calculateProjectsTotalTime
{
	CGFloat totalTime = 0.0;
	for (ProjectTimesheet *  project in self.clientTimesheet.projects)
	{
		totalTime += project.totalTime;
	}
	self.totalTimeLabel.title = [NSDate timeFromInterval:totalTime];
}

-(void)calculateProjectTotalTimeWithSection:(int)section
{
	ProjectTimesheet * projectTimesheet = [self.clientTimesheet.projects objectAtIndex:section];
	GTFontLabel * projectSectionTotalTimeLabel = [_projectSectionTotalTimeLabels objectAtIndex:section];
	projectSectionTotalTimeLabel.text = [NSDate timeFromInterval:projectTimesheet.totalTime];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return self.clientTimesheet.projects.count;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	ProjectTimesheet * projectTimesheet = [self.clientTimesheet.projects objectAtIndex:section];
	
	UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.bounds.size.width, 20)];
	view.backgroundColor = [UIColor gtLightGrey];
	
	GTFontLabel * projectSectionName = [[GTFontLabel alloc] initWithFrame:
									  CGRectMake(15, 1, 200, 20) font:[UIFont fontWithName:kGTFontBold size:kGTFontSizeSmall]];
	projectSectionName.text = projectTimesheet.project.name;
	[view addSubview:projectSectionName];
	
	GTFontLabel * projectSectionTotalTimeLabel = [[GTFontLabel alloc]
												  initWithFrame:CGRectMake(self.tableView.bounds.size.width - 107, 1, 100, 20)
												  font:[UIFont fontWithName:kGTFontBold size:kGTFontSizeSmall]];
	projectSectionTotalTimeLabel.text = [NSDate timeFromInterval:projectTimesheet.totalTime];
	projectSectionTotalTimeLabel.textAlignment = NSTextAlignmentRight;
	[view addSubview:projectSectionTotalTimeLabel];
	
	[_projectSectionTotalTimeLabels insertObject:projectSectionTotalTimeLabel atIndex:section];
	
	return view;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	ProjectTimesheet * projectTimesheet = [self.clientTimesheet.projects objectAtIndex: section];
	return [projectTimesheet timesheetCount];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	GTTimeSheetCell * cell = (GTTimeSheetCell *) [tableView dequeueReusableCellWithIdentifier:@"TimesheetCell"];
	
	ProjectTimesheet * projectTimesheet = [self.clientTimesheet.projects objectAtIndex:indexPath.section];
	Timesheet * timesheet = [projectTimesheet timesheetAtIndex:indexPath.row];
	
	cell.dateLabel.text         = [timesheet.startTime preferredDateFormat];
	cell.startTimeLabel.text    = [timesheet.startTime timeFormatted];
	cell.endTimeLabel.text      = [timesheet.endTime timeFormatted];
	cell.timeIntervalLabel.text = [NSDate timeFromInterval:[timesheet.endTime timeIntervalSinceDate:timesheet.startTime]];
	
	return cell;
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
	return true;
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (editingStyle == UITableViewCellEditingStyleDelete)
	{
		[self showHUD:MBProgressHUDModeIndeterminate label:NSLocalizedString(@"Deleting", nil)];
		
		NSArray * projects = self.clientTimesheet.projects;
		ProjectTimesheet * projectTimesheet = [projects objectAtIndex:indexPath.section];
		
		Timesheet * timesheet = [projectTimesheet timesheetAtIndex:indexPath.row];
		[[GTTimesheetServices instance] deleteTimesheet:timesheet.objectId
												  block:^(bool success, NSError * error)
		{
			if (!error)
			{
				[projectTimesheet removeTimesheetAtIndex:indexPath.row];
				[self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
									  withRowAnimation:UITableViewRowAnimationTop];
				[self calculateProjectsTotalTime];
				[self calculateProjectTotalTimeWithSection:indexPath.section];
				[self hideHUD];
			}
		}];
	}
}

@end
