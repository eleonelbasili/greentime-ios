//
//  GTTimesheetSearchCell.h
//  GreenTime
//
//  Created by Eduardo Dias on 26/11/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GTFontLabel.h"

@interface GTTimesheetSearchCell : UITableViewCell

@property (strong, nonatomic) IBOutlet GTFontLabel * clientName;
@property (strong, nonatomic) IBOutlet GTFontLabel * clientHours;

@end
