//
//  GTTimesheetSearchViewController.h
//  GreenTime
//
//  Created by Eduardo Dias on 11/11/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GTBaseViewController.h"
#import "GTCalendar.h"
#import "GTFontLabel.h"
#import "ClientTimesheet.h"
#import "GTPlaceHolderView.h"

extern NSString * const kGTimesheetClientViewController;

@interface GTTimesheetSearchViewController : GTBaseViewController
<GTCalendarDelegate, UITableViewDataSource, UITableViewDelegate>

-(IBAction)toggleCalendar:(id)sender;
-(IBAction)addNewTimesheet:(id)sender;

@property (strong, nonatomic) IBOutlet UIBarButtonItem * plusButton;
@property (strong, nonatomic) IBOutlet UIView * totalView;
@property (strong, nonatomic) IBOutlet GTFontLabel * dateRangeLabel;
@property (strong, nonatomic) IBOutlet UILabel * totalTimeLabel;
@property (strong, nonatomic) IBOutlet UITableView * tableView;

@property (strong, nonatomic) UIRefreshControl * refreshControl;
@property (strong, nonatomic) NSDate * startDate;
@property (strong, nonatomic) NSDate * endDate;

@end
