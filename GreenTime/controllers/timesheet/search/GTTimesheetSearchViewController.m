//
//  GTTimesheetSearchViewController.m
//  GreenTime
//
//  Created by Eduardo Dias on 11/11/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import "GTTimesheetSearchViewController.h"
#import "GTTimesheetClientViewController.h"
#import "GTTimesheetServices.h"
#import "GTTimesheetSearchCell.h"
#import "GTFontLabel.h"
#import "ClientTimesheet.h"
#import "ProjectTimesheet.h"

#import <ReactiveCocoa/ReactiveCocoa.h>

NSString * const kGTimesheetClientViewController  = @"TimesheetClientViewController";

@interface GTTimesheetSearchViewController ()

@end

@implementation GTTimesheetSearchViewController
{
	NSMutableArray * _clientsTimesheet;
	
	GTCalendar * _calendar;
	GTPlaceHolderView * _emptyView;
	
	BOOL _isNewTimesheetRequired;
}

#pragma mark - Timesheet Initialization

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	// Adding refresh control
	self.refreshControl = [[UIRefreshControl alloc] init];
	[self.refreshControl addTarget:self action:@selector(refreshQuery) forControlEvents:UIControlEventValueChanged];
	[self.tableView addSubview:self.refreshControl];
	
	// Adding empty place holder
	_emptyView = [[GTPlaceHolderView alloc] initWithFrame:self.view.frame viewType:kGTTimesheetPlaceHolder];
	[self.view addSubview:_emptyView];
	
	// Setting total view bar
	self.dateRangeLabel.font = [UIFont fontWithName:kGTFontBold size:kGTFontSizeSmall];
	self.totalTimeLabel.font = [UIFont fontWithName:kGTFontBold size:kGTFontSizeSmall];
	[self.totalView setHidden:YES];
	
    // Removing separator to empty rows
	UIView * view = [[UIView alloc] init];
	view.backgroundColor = [UIColor clearColor];
	[self.tableView setTableFooterView:view];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
	[self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:animated];
	
	[self refreshQuery];
}

-(void)refreshQuery
{
	if (self.startDate && self.endDate)
	{
		[self findClientsWithTimesheet:self.startDate endDay:self.endDate];
	}
	else
	{
		[self.refreshControl endRefreshing];
	}
}

#pragma mark - Table View operations

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return _clientsTimesheet.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * CellIdentifier = @"ProjectTimesheetCell";
	GTTimesheetSearchCell * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	
	ClientTimesheet * clientTimesheet = [_clientsTimesheet objectAtIndex:indexPath.row];
	cell.clientName.text = clientTimesheet.client.name;
	cell.clientHours.text = [NSDate timeFromInterval:clientTimesheet.totalTime];
	
    return cell;
}


#pragma mark - Calendar Operations

- (void)selectedDates:(NSDate *)startDate endDate:(NSDate *)endDate
{
	[self toggleCalendar:nil];
	
	if(!startDate || !endDate)return;
	
	self.startDate = startDate;
	self.endDate = endDate;
	[self findClientsWithTimesheet:startDate endDay:endDate];
}

-(void)findClientsWithTimesheet:(NSDate *)startDate endDay:(NSDate *)endDate
{
	[self.refreshControl beginRefreshing];
	
	[[GTTimesheetServices instance] clientsTimesheetInDateRange:startDate
															endDate:endDate
															  block:^(NSArray * clientsTimesheetDict, NSError *error)
	{
		_clientsTimesheet = [[NSMutableArray alloc] init];
		for (NSDictionary * clientTimesheetDict in clientsTimesheetDict)
		{
			ClientTimesheet * clientTimesheet = [[ClientTimesheet alloc]
												 initWithDictionary:clientTimesheetDict];
			[_clientsTimesheet addObject:clientTimesheet];
		}
		
		if (clientsTimesheetDict.count > 0)
		{
			CGFloat totalTime = 0.0;
			for (ClientTimesheet * clientTimesheet in _clientsTimesheet)
			{
				totalTime += clientTimesheet.totalTime;
			}
			self.dateRangeLabel.text = [NSDate dateRangeWithDates:startDate endDate:endDate];
			self.totalTimeLabel.text = [NSDate timeFromInterval:totalTime];
		}
		
		_emptyView.hidden      = (clientsTimesheetDict.count > 0);
		self.totalView.hidden = (clientsTimesheetDict.count == 0);
		
		[self.tableView reloadData];
		[self.refreshControl endRefreshing];
	}];
}

- (IBAction)toggleCalendar:(id)sender
{
	if (!_calendar)
	{
		CGFloat tabBarHeight = self.tabBarController.tabBar.frame.size.height;
		CGFloat navBarHeight = self.navigationController.navigationBar.frame.size.height;
		CGFloat statusBarHeight = [[UIApplication sharedApplication] statusBarFrame].size.height;
		CGFloat calendarHeight = self.view.superview.frame.size.height - navBarHeight - statusBarHeight - tabBarHeight;
		
		CGRect initFrame = CGRectMake(0, - calendarHeight, 320, calendarHeight);
		CGRect finalFrame = CGRectMake(0, navBarHeight + statusBarHeight, 320, calendarHeight);
		
		_calendar = [[GTCalendar alloc] initWithDelegate:self initFrame:initFrame finalFrame:finalFrame];
		
		[self.view.superview.superview addSubview:_calendar];
	}
	[_calendar toggle];
}


#pragma mark - Calendar delegates

-(void)calendarDidShow{}

-(void)calendarDidHide
{
	if (_isNewTimesheetRequired)
	{
		_isNewTimesheetRequired = false;
		[self performSegueWithIdentifier:@"NewTimesheet" sender:self];
	}
}


#pragma mark - Helpers

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if ([segue.identifier isEqualToString:kGTimesheetClientViewController])
	{
		NSIndexPath * indexPath = [self.tableView indexPathForSelectedRow];
        GTTimesheetClientViewController * timesheetCVC = segue.destinationViewController;
        timesheetCVC.clientTimesheet = [_clientsTimesheet objectAtIndex:indexPath.row];
		timesheetCVC.startDate = self.startDate;
		timesheetCVC.endDate = self.endDate;
    }
}

- (IBAction)addNewTimesheet:(id)sender
{
	if (_calendar && _calendar.isVisible)
	{
		_isNewTimesheetRequired = true;
		[_calendar hide];
	}
	else
	{
		[self performSegueWithIdentifier:@"NewTimesheet" sender:self];
	}
}

@end
