//
//  GTLocationManager.h
//  GreenTime
//
//  Created by Eduardo Dias on 3/11/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "GTLocationManagerDelegate.h"

extern const int kGTMaxLocationsAvailable;

extern NSString * const kGTDidEnterRegion;
extern NSString * const kGTDidExitRegion;
extern NSString * const kGTDidStartMonitoringRegion;
extern NSString * const kGTDidStopMonitoringRegion;

typedef void (^FindCurrentLocationBlock)(CLCircularRegion * region);

@interface GTLocationManager : NSObject <CLLocationManagerDelegate>

+(GTLocationManager *)instance;

@property(nonatomic, weak)id <GTLocationManagerDelegate> delegate;

-(void)updateCurrentLocation;
-(void)updateMonitoringSteps;
-(void)stopMonitoringSteps;
-(void)startMonitoringCurrentLocationWithKey:(NSString *)key;
-(void)stopMonitoringCurrentLocationWithKey:(NSString *)key;
-(void)findCurrentLocationInMonitoredRegions:(FindCurrentLocationBlock)block;
-(int)availableLocations;
-(CLCircularRegion *)findMonitoredRegionWithKey:(NSString *)key;
-(void)stopMonitoringRegions;


@end
