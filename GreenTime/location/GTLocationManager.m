//
//  GTLocationManager.m
//  GreenTime
//
//  Created by Eduardo Dias on 3/11/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import "GTLocationManager.h"
#import "Client.h"

int const kGTMaxLocationsAvailable = 20;

// Notifications
NSString * const kGTDidEnterRegion           = @"didEnterRegion";
NSString * const kGTDidExitRegion            = @"didExitRegion";
NSString * const kGTDidStartMonitoringRegion = @"didStartMonitoringRegion";
NSString * const kGTDidStopMonitoringRegion  = @"didStopMonitoringRegion";

// Start Monitoring Operations
static int const kGTCheckCurrentLocationIsMonitored = 1;
static int const kGTStopMonitoringCurrentLocation   = 2;
static int const kGTCheckLocationKeyIsAvailable     = 3;
static int const kGTStopMonitoringLocationKey       = 4;
static int const kGTStartMonitoringCurrentLocation  = 5;

// Search Operations
static int const kGTFindLocation                    = 6;
static int const kGTFindLocationInMonitoredRegions  = 7;

@implementation GTLocationManager
{
	NSString * _regionId;
	
	CLLocationManager * _locationManager;
	CLCircularRegion *  _currentLocation;
	
	FindCurrentLocationBlock _findCurrentLocation;
	
	int _nextMonitoringStep;
	int _currentOperation;
	
	bool _currentLocationIsMonitored;
	bool _locationKeyIsAvailable;
}

#pragma mark - Initialization

+(GTLocationManager *)instance
{
	static GTLocationManager * gtLocation = nil;
	
	if (!gtLocation)
	{
		gtLocation = [[super allocWithZone:nil] init];
		[gtLocation initializeLocationManager];
	}
	return gtLocation;
}

+(id)allocWithZone:(struct _NSZone *)zone
{
	return [self instance];
}

-(void)initializeLocationManager
{
	_locationManager = [[CLLocationManager alloc] init];
	[_locationManager setDelegate:self];
	[_locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
}


#pragma mark - Updating location

-(void)updateCurrentLocation
{
	[self findCurrentLocationInMonitoredRegions:^(CLCircularRegion * region)
	{
		if (!_currentLocation && region)
		{
			_currentLocation = region;
			
			NSDictionary * userInfo = [NSDictionary dictionaryWithObject:region.identifier forKey:@"regionId"];
			[[NSNotificationCenter defaultCenter] postNotificationName:kGTDidEnterRegion object:nil userInfo:userInfo];
		}
		else if (_currentLocation && !region)
		{
			NSDictionary * userInfo = [NSDictionary dictionaryWithObject:_currentLocation.identifier forKey:@"regionId"];
			[[NSNotificationCenter defaultCenter] postNotificationName:kGTDidExitRegion object:nil userInfo:userInfo];
			
			_currentLocation = nil;
		}
	}];
}


#pragma mark - Starting Operations

-(void)stopMonitoringSteps
{
	[self resetMonitoringParameters];
}

-(void)resetMonitoringParameters
{
	_regionId = nil;
	_nextMonitoringStep = kGTCheckCurrentLocationIsMonitored;
	_currentLocation = nil;
	_currentLocationIsMonitored = false;
	_locationKeyIsAvailable = false;
}

-(void)updateMonitoringSteps
{
	switch (_nextMonitoringStep)
	{
		case kGTCheckCurrentLocationIsMonitored:
			 _nextMonitoringStep = kGTStopMonitoringCurrentLocation;
			 [self checkCurrentLocationIsMonitored];
		break;
		case kGTStopMonitoringCurrentLocation:
			 _nextMonitoringStep = kGTCheckLocationKeyIsAvailable;
			[self stopMonitoringCurrentLocation];
		break;
		case kGTCheckLocationKeyIsAvailable:
			_nextMonitoringStep = kGTStopMonitoringLocationKey;
			[self checkLocationKeyIsAvailable];
		break;
		case kGTStopMonitoringLocationKey:
			_nextMonitoringStep = kGTStartMonitoringCurrentLocation;
			[self stopMonitoringLocationKey];
		break;
		case kGTStartMonitoringCurrentLocation:
			_nextMonitoringStep = -1;
			[self startMonitoringCurrentLocation];
		break;
	}
}

-(void)didFindCurrentLocation:(CLCircularRegion *)location
{
	_currentLocation = location;
	[self updateMonitoringSteps];
}

-(void)checkCurrentLocationIsMonitored
{
	NSSet * monitoredRegions = [_locationManager monitoredRegions];
	for (CLCircularRegion * region in monitoredRegions)
	{
		NSNumber * distance = [self calculateDistanceInMetersBetweenCoord:region.center coord:_currentLocation.center];
		if (distance.doubleValue < kGTDistanceToMatchLocation)
		{
			_currentLocationIsMonitored = true;
			[self.delegate currentLocationWasMonitored:region];
			
			return;
		}
	}
	[self updateMonitoringSteps];
}

-(void)stopMonitoringCurrentLocation
{
	if (_currentLocationIsMonitored)
	{
		[self stopMonitoringCurrentLocationWithKey:_regionId];
	}
	[self updateMonitoringSteps];
}

-(void)checkLocationKeyIsAvailable
{
	NSSet * monitoredRegions = [_locationManager monitoredRegions];
	NSSet * regionSet = [monitoredRegions filteredSetUsingPredicate:[NSPredicate predicateWithFormat:@"self.identifier contains[CD] %@", _regionId]];
	
	NSArray * regions = [regionSet allObjects];
	if (regions.count > 0)
	{
		[self.delegate locationKeyWasNotAvailable:_regionId];
	}
	else
	{
		[self updateMonitoringSteps];
	}
}

-(void)stopMonitoringLocationKey
{
	if(!_locationKeyIsAvailable)
	{
		[self stopMonitoringCurrentLocationWithKey:_regionId];
	}
	[self updateMonitoringSteps];
}

-(void)startMonitoringCurrentLocation
{
	_nextMonitoringStep = kGTCheckCurrentLocationIsMonitored;
	[_locationManager startMonitoringForRegion:_currentLocation];
	
	[self resetMonitoringParameters];
}

-(void)startMonitoringCurrentLocationWithKey:(NSString *)key;
{
	_regionId = key;
	
	_currentOperation = kGTFindLocation;
	_nextMonitoringStep = kGTCheckCurrentLocationIsMonitored;
	[_locationManager startUpdatingLocation];
}

-(void)stopMonitoringRegions
{
	NSSet * monitoredRegions = [_locationManager monitoredRegions];
	for (CLCircularRegion * region in monitoredRegions)
	{
		[_locationManager stopMonitoringForRegion:region];
	}
}


#pragma mark - Stopping Operations

-(void)stopMonitoringCurrentLocationWithKey:(NSString *)key
{
	NSSet * monitoredRegions = [_locationManager monitoredRegions];
	NSSet * regionSet = [monitoredRegions filteredSetUsingPredicate:[NSPredicate predicateWithFormat:@"self.identifier == %@", key]];
	
	NSArray * regions = [regionSet allObjects];
	
	if(regions.count > 0)
	{
		CLCircularRegion * region = [regions objectAtIndex:0];
		[_locationManager stopMonitoringForRegion:region];
		
		NSDictionary * userInfo = [NSDictionary dictionaryWithObject:region.identifier forKey:@"regionId"];
		[[NSNotificationCenter defaultCenter] postNotificationName:kGTDidStopMonitoringRegion object:nil userInfo:userInfo];
	}
}

-(void)stopMonitoringAllRegions
{
	NSSet * monitoredRegions = [_locationManager monitoredRegions];
	for (CLCircularRegion * region in monitoredRegions)
	{
		[_locationManager stopMonitoringForRegion:region];
	}
}


#pragma mark - Searching Operations

-(void)findCurrentLocationInMonitoredRegions:(FindCurrentLocationBlock)block;
{
	if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorized && [CLLocationManager locationServicesEnabled])
	{
		_findCurrentLocation = block;
		_currentOperation = kGTFindLocationInMonitoredRegions;
		[_locationManager startUpdatingLocation];
	}
	else
	{
		block(nil);
	}
}

-(void)findLocationInMonitoredRegions:(CLCircularRegion *)location
{
	CLCircularRegion * foundRegion;
	
	NSSet * monitoredRegions = [_locationManager monitoredRegions];
	for (CLCircularRegion * region in monitoredRegions)
	{
		NSNumber * numDistance = [self calculateDistanceInMetersBetweenCoord:region.center coord:location.center];
		int intDistance = round(numDistance.doubleValue);
		if (intDistance < kGTDistanceToMatchLocation)
		{
			foundRegion = region;
			break;
		}
	}
	_findCurrentLocation(foundRegion);
}

-(CLCircularRegion *)findMonitoredRegionWithKey:(NSString *)key
{
	NSSet * monitoredRegions = [_locationManager monitoredRegions];
	NSSet * regionSet = [monitoredRegions filteredSetUsingPredicate:[NSPredicate predicateWithFormat:@"self.identifier contains[CD] %@", key]];
	
	NSArray * regions = [regionSet allObjects];
	if (regions.count > 0)
	{
		CLCircularRegion * region = (CLCircularRegion *)[[regionSet allObjects] objectAtIndex:0];
		return region;
	}
	return nil;
}


#pragma mark - Delegates

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
	CLLocation * lastLocation = [locations lastObject];
	NSTimeInterval interval = [[lastLocation timestamp] timeIntervalSinceNow];
	if (interval > -30 && interval < 0)
	{
		[_locationManager stopUpdatingLocation];
		
		CLLocationCoordinate2D location = CLLocationCoordinate2DMake(lastLocation.coordinate.latitude, lastLocation.coordinate.longitude);
		NSString * defaultRegionId = (_regionId) ? _regionId : @"default";
		CLCircularRegion * region = [[CLCircularRegion alloc] initWithCenter:location radius:1 identifier:defaultRegionId];
		
		switch (_currentOperation)
		{
			case kGTFindLocation:
				[self didFindCurrentLocation:region];
			break;
			case kGTFindLocationInMonitoredRegions:
				[self findLocationInMonitoredRegions:region];
			break;
		}
		_currentOperation = -1;
		
		NSLog(@"%.4f, %.4f", lastLocation.coordinate.latitude, lastLocation.coordinate.longitude);
	}
}

-(NSNumber *)calculateDistanceInMetersBetweenCoord:(CLLocationCoordinate2D)coord1 coord:(CLLocationCoordinate2D)coord2
{
    NSInteger nRadius = 6371; // Earth's radius in Kilometers
    double latDiff = (coord2.latitude - coord1.latitude) * (M_PI/180);
    double lonDiff = (coord2.longitude - coord1.longitude) * (M_PI/180);
    
    double lat1InRadians = coord1.latitude *  (M_PI/180);
    double lat2InRadians = coord2.latitude *  (M_PI/180);
    
    double nA = pow ( sin(latDiff/2), 2 ) + cos(lat1InRadians) * cos(lat2InRadians) * pow ( sin(lonDiff/2), 2 );
    double nC = 2 * atan2( sqrt(nA), sqrt( 1 - nA ));
    double nD = nRadius * nC;
    
    // convert to meters
    return @(nD * 1000);
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
	NSLog(@"Error: %@ %@", error, [error userInfo]);
}

-(void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region
{
	NSDictionary * userInfo = [NSDictionary dictionaryWithObject:region.identifier forKey:@"regionId"];
	[[NSNotificationCenter defaultCenter] postNotificationName:kGTDidEnterRegion object:nil userInfo:userInfo];
}

-(void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region
{
	NSDictionary * userInfo = [NSDictionary dictionaryWithObject:region.identifier forKey:@"regionId"];
	[[NSNotificationCenter defaultCenter] postNotificationName:kGTDidExitRegion object:nil userInfo:userInfo];
}

-(void)locationManager:(CLLocationManager *)manager monitoringDidFailForRegion:(CLRegion *)region withError:(NSError *)error
{
	UIAlertView * alertView = [[UIAlertView alloc]
							   initWithTitle:@"Location Service"
							   message:@"Location Fail"
							   delegate:self
							   cancelButtonTitle:@"Cancel"
							   otherButtonTitles:@"OK", nil];
	
	[alertView show];
}

- (void)locationManager:(CLLocationManager *)manager didStartMonitoringForRegion:(CLRegion *)region
{
	NSDictionary * userInfo = [NSDictionary dictionaryWithObject:region.identifier forKey:@"regionId"];
	[[NSNotificationCenter defaultCenter] postNotificationName:kGTDidStartMonitoringRegion object:nil userInfo:userInfo];
}


-(int)availableLocations
{
	NSSet * monitoredRegions = [_locationManager monitoredRegions];
	return (kGTMaxLocationsAvailable - monitoredRegions.count);
}

@end