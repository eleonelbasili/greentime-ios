//
//  GTLocationManagerDelegate.h
//  GreenTime
//
//  Created by Eduardo Dias on 8/11/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol GTLocationManagerDelegate <NSObject>

-(void)currentLocationWasMonitored:(CLCircularRegion *)region;
-(void)locationKeyWasNotAvailable:(NSString *)locationKey;

@end
