//
//  main.m
//  GreenTime
//
//  Created by Eduardo Dias on 25/08/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "GTAppDelegate.h"

int main(int argc, char * argv[])
{
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([GTAppDelegate class]));
	}
}
