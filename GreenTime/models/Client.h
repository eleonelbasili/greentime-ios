//
//  Client.h
//  GreenTime
//
//  Created by Eduardo Dias on 15/09/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>

@class Project;

@interface Client : PFObject <PFSubclassing>

// Global properties
@property (retain) PFUser   * user;
@property (retain) NSString * name;
@property (retain) NSString * phone;
@property (retain) NSString * email;
@property (retain) PFFile   * image;
@property (retain) NSString * website;
@property (retain) NSString * notes;
@property (retain) NSString * hourlyRate;

+ (NSString *) parseClassName;
- (void) addProject:(Project *) project;
- (void) addProjects:(NSArray *) projects;
- (Project *) projectAtIndex:(int)index;
- (void) removeProjectAtIndex:(int)index;
- (int) projectsCount;
- (NSDictionary *) toDictionary;

@end
