//
//  Client.m
//  GreenTime
//
//  Created by Eduardo Dias on 15/09/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import "Client.h"
#import "Project.h"
#import <Parse/Parse.h>
#import <Parse/PFObject+Subclass.h>
#import <objc/runtime.h>

@implementation Client
{
	NSMutableArray * _projects;
}

@dynamic user;
@dynamic name;
@dynamic phone;
@dynamic email;
@dynamic image;
@dynamic website;
@dynamic notes;
@dynamic hourlyRate;

#pragma - Initialization

-(id)init
{
	self = [super init];
	if (self)
	{
		_projects  = [[NSMutableArray alloc] init];
	}
	return self;
}

+(NSString *)parseClassName
{
	return @"Client";
}


-(NSString *)description
{
	return self.name;
}


#pragma - Project methods

- (void) addProject:(Project *) project
{
	[self sortProjectsByName];
	[_projects addObject:project];
}

- (void) addProjects:(NSArray *) projects
{
	[self sortProjectsByName];
	_projects = [[NSMutableArray alloc] initWithArray:projects];
}

- (Project *) projectAtIndex:(int)index
{
	[self sortProjectsByName];
	return [_projects objectAtIndex:index];
}

- (void) removeProjectAtIndex:(int)index
{
	[self sortProjectsByName];
	[_projects removeObjectAtIndex:index];
}

-(void)sortProjectsByName
{
	NSSortDescriptor * nameDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
	NSArray * sortDescriptors = @[nameDescriptor];
	[_projects sortUsingDescriptors:sortDescriptors];
}

- (int) projectsCount
{
	return _projects.count;
}


#pragma - Helper methods

-(NSDictionary *) toDictionary
{
	NSMutableDictionary * dictionary = [[NSMutableDictionary alloc] init];
	if (self.objectId != nil)
	{
		[dictionary setObject:self.objectId forKey:@"id"];
	}
	
	NSArray * keys = [self allKeys];
	for (NSString * key in keys)
	{
		id keyValue = [self objectForKey:key];
		if (keyValue == nil)
			continue;

		if ([key isEqualToString:@"user"])
		{
			[dictionary setObject:[keyValue objectId] forKey:@"userId"];
		}
		else if ([key isEqualToString:@"image"])
		{
			NSData * imageData  = [self.image getData];
			if (imageData != nil)
			{
				NSString * imageStr = [imageData
									  base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
				[dictionary setObject:imageStr forKey:key];
			}
		}
		else
		{
			[dictionary setObject:keyValue forKey:key];
		}
	}
	if (_projects.count > 0)
	{
		NSMutableArray * projectsDict = [[NSMutableArray alloc] init];
		for (Project * project in _projects)
		{
			[projectsDict addObject:[project toDictionary]];
		}
		[dictionary setObject:[projectsDict copy] forKey:@"projects"];
	}
	
	return dictionary;
}

@end
