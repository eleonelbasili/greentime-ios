//
//  ClientModel.h
//  GreenTime
//
//  Created by Eduardo Dias on 22/10/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Client.h"

@interface ClientModel : NSObject
{
	Client * clientInRecordingMode;
}

+(ClientModel *)instance;

-(void)setClientInRecordingMode:(Client *)client;
-(BOOL)isClientInRecordingMode:(Client *)client;

@end
