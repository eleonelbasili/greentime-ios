//
//  ClientModel.m
//  GreenTime
//
//  Created by Eduardo Dias on 22/10/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import "ClientModel.h"

@implementation ClientModel


#pragma - Initialization

+(ClientModel *)instance
{
	static ClientModel * clientModel = nil;
	if (!clientModel)
	{
		clientModel = [[super allocWithZone:nil] init];
	}
	return clientModel;
}

+(id)allocWithZone:(struct _NSZone *)zone
{
	return [self instance];
}


#pragma - Operations

-(void)setClientInRecordingMode:(Client *)client
{
	clientInRecordingMode = client;
}

-(BOOL)isClientInRecordingMode:(Client *)client
{
	return (clientInRecordingMode && [clientInRecordingMode.objectId isEqualToString:client.objectId]);
}

@end
