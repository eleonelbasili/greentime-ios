//
//  ClientTime.h
//  GreenTime
//
//  Created by Eduardo Dias on 15/11/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Timesheet.h"
#import "Client.h"

@interface ClientTimesheet : NSObject

@property (nonatomic, strong) Client * client;
@property (nonatomic, strong) NSMutableArray * projects;
@property (nonatomic) CGFloat totalTime;

-(id)initWithDictionary:(NSDictionary *)dictionary;

@end
