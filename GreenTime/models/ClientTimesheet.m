//
//  ClientTime.m
//  GreenTime
//
//  Created by Eduardo Dias on 15/11/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import "ClientTimesheet.h"

@implementation ClientTimesheet

-(id)init
{
	self = [super init];
	if (self)
	{
		self.projects = [[NSMutableArray alloc] init];
	}
	return self;
}

-(id)initWithDictionary:(NSDictionary *)dictionary
{
	self = [super init];
	if (self)
	{
		self.client = [dictionary objectForKey:@"client"];
		self.projects = [[NSMutableArray alloc] init];
		self.totalTime = [[dictionary objectForKey:@"totalTime"] floatValue];
	}
	return self;
}

@end
