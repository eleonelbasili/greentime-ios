//
//  Project.h
//  GreenTime
//
//  Created by Eduardo Dias on 1/09/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import <Parse/Parse.h>

@class Client;

@interface Project : PFObject <PFSubclassing>

@property (retain) Client   * client;
@property (retain) NSString * name;
@property (retain) NSString * notes;

+ (NSString *) parseClassName;
- (id)initWithClient:(Client *)client;
- (NSDictionary *) toDictionary;

@end


