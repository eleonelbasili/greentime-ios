//
//  Project.m
//  GreenTime
//
//  Created by Eduardo Dias on 1/09/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import "Project.h"
#import <Parse/Parse.h>
#import <Parse/PFObject+Subclass.h>

NSString * const ProjectsLoaded  = @"projectsLoaded";

@implementation Project 

@dynamic client;
@dynamic name;
@dynamic notes;

+ (NSString *)parseClassName
{
	return @"Project";
}

- (id)initWithClient:(Client *)client;
{
    self = [super init];
    if (self)
	{
        self.client = client;
    }
    return self;
}

- (NSString *)description
{
	return self.name;
}

-(NSDictionary *) toDictionary
{
	NSMutableDictionary * dictionary = [[NSMutableDictionary alloc] init];
	
	if (self.objectId != nil)
	{
		[dictionary setObject:self.objectId forKey:@"id"];
	}
	
	NSArray * keys = [self allKeys];
	for (NSString * key in keys)
	{
		id keyValue = [self objectForKey:key];
		if (keyValue == nil)
			continue;
		
		if([key isEqualToString:@"client"])
		{
			if ([keyValue objectId] != nil)
			{
				[dictionary setObject:[keyValue objectId] forKey:@"clientId"];
			}
			else
			{
				continue;
			}
		}
		else
		{
			[dictionary setObject:keyValue forKey:key];
		}
	}
	return dictionary;
}

@end
