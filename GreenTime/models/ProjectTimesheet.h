//
//  ProjectTime.h
//  GreenTime
//
//  Created by Eduardo Dias on 15/11/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Timesheet.h"
#import "Project.h"

@interface ProjectTimesheet : NSObject

@property (nonatomic, strong) Project * project;
@property (nonatomic) CGFloat totalTime;

-(id)initWithDictionary:(NSDictionary *)dictionary;
-(Timesheet *)timesheetAtIndex:(int)index;
-(int)timesheetCount;
-(void)removeTimesheetAtIndex:(int)index;

@end
