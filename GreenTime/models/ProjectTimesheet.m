//
//  ProjectTime.m
//  GreenTime
//
//  Created by Eduardo Dias on 15/11/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import "ProjectTimesheet.h"

@implementation ProjectTimesheet
{
	NSMutableArray * _timesheets;
}
-(id)init
{
	self = [super init];
	if (self)
	{
		_timesheets = [[NSMutableArray alloc] init];
	}
	return self;
}

-(id)initWithDictionary:(NSDictionary *)dictionary
{
	self = [super init];
	if (self)
	{
		self.project = [dictionary objectForKey:@"project"];
		self.totalTime = [[dictionary objectForKey:@"totalTime"] floatValue];
		_timesheets = [[NSMutableArray alloc] initWithArray:[dictionary objectForKey:@"timesheets"]];
	}
	return self;
}

-(Timesheet *)timesheetAtIndex:(int)index
{
	return [_timesheets objectAtIndex:index];
}

-(int)timesheetCount
{
	return [_timesheets count];
}

-(void)removeTimesheetAtIndex:(int)index
{
	Timesheet * timesheet = [_timesheets objectAtIndex:index];
	[_timesheets removeObjectAtIndex:index];
	self.totalTime -= [timesheet totalTime];
}

@end
