//
//  Timesheet.h
//  GreenTime
//
//  Created by Eduardo Dias on 26/10/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>
#import "Client.h"
#import "Project.h"

@interface Timesheet : PFObject <PFSubclassing>

@property (retain) Project * project;
@property (retain) NSDate * startTime;
@property (retain) NSDate * endTime;

+ (NSString *)parseClassName;

-(id)initWithProject:(Project *)project;
-(NSDictionary *) toDictionary;

-(double)totalTime;

@end
