//
//  Timesheet.m
//  GreenTime
//
//  Created by Eduardo Dias on 26/10/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import "Timesheet.h"
#import <Parse/PFObject+Subclass.h>

@implementation Timesheet

@dynamic project;
@dynamic startTime;
@dynamic endTime;

+(NSString *)parseClassName
{
	return @"Timesheet";
}

-(id)initWithProject:(Project *)project
{
	self = [super init];
	if (self)
	{
		self.project = project;
	}
	return self;
}

-(NSDictionary *) toDictionary
{
	NSMutableDictionary * dictionary = [[NSMutableDictionary alloc] init];
	NSArray * keys = [self allKeys];
	for (NSString * key in keys)
	{
		id keyValue = [self objectForKey:key];
		if (keyValue == nil)
			continue;
		
		if ([key isEqualToString:@"project"])
		{
			[dictionary setObject:[keyValue objectId] forKey:@"projectId"];
		}
		else
		{
			[dictionary setObject:keyValue forKey:key];
		}
	}
	return dictionary;
}

-(double)totalTime
{
	NSTimeInterval timeDifference = [self.endTime timeIntervalSinceDate:self.startTime];
	return timeDifference;
}

@end
