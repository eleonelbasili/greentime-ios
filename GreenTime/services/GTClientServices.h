//
//  GTClientServices.h
//  GreenTime
//
//  Created by Eduardo Dias on 14/01/14.
//  Copyright (c) 2014 com.dymextech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Client.h"

typedef void (^SaveClientBlock)(Client * client, NSError * error);
typedef void (^DeleteClientBlock)(Client * client, NSError * error);
typedef void (^FindClientsBlock)(NSArray * clients, NSError * error);

@interface GTClientServices : NSObject

+(GTClientServices *)instance;

-(void)saveClient:(NSDictionary *)parameters block:(SaveClientBlock)block;
-(void)deleteClient:(NSString *)clientId block:(DeleteClientBlock)block;
-(void)findClientsWithBlock:(FindClientsBlock)block;

@end
