//
//  GTClientServices.m
//  GreenTime
//
//  Created by Eduardo Dias on 14/01/14.
//  Copyright (c) 2014 com.dymextech. All rights reserved.
//

#import "GTClientServices.h"

@implementation GTClientServices

#pragma - Initialization

+(GTClientServices *)instance
{
	static GTClientServices * clientServices = nil;
	if (!clientServices)
	{
		clientServices = [[super allocWithZone:nil] init];
	}
	return clientServices;
}

+(id)allocWithZone:(struct _NSZone *)zone
{
	return [self instance];
}


#pragma - Operations

-(void)saveClient:(NSDictionary *)params block:(SaveClientBlock)block
{
	[PFCloud callFunctionInBackground:@"saveClient"
					   withParameters:params
								block:^(Client * client, NSError * error)
	 {
		 block(client, error);
		 NSLog(@"Error: %@ %@", error, [error userInfo]);
	 }];
}

-(void)deleteClient:(NSString *)clientId block:(DeleteClientBlock)block
{
	[PFCloud callFunctionInBackground:@"deleteClient"
					   withParameters:@{@"clientId":clientId}
								block:^(Client * client, NSError * error)
	 {
		 block(client, error);
		 NSLog(@"Error: %@ %@", error, [error userInfo]);
	 }];
}

-(void)findClientsWithBlock:(FindClientsBlock)block
{
	[PFCloud callFunctionInBackground:@"findClients"
					   withParameters:@{}
								block:^(NSArray * clients, NSError * error)
	 {
		 block(clients, error);
		 NSLog(@"Error: %@ %@", error, [error userInfo]);
	 }];
}

@end
