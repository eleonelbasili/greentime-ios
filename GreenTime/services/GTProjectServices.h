//
//  GTProjectServices.h
//  GreenTime
//
//  Created by Eduardo Dias on 27/01/14.
//  Copyright (c) 2014 com.dymextech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Client.h"

typedef void (^FindProjectsBlock)(NSArray * projects, NSError * error);

@interface GTProjectServices : NSObject

+(GTProjectServices *)instance;
-(void)findProjectsWithClientId:(NSString *)clientId block:(FindProjectsBlock)block;

@end
