//
//  GTProjectServices.m
//  GreenTime
//
//  Created by Eduardo Dias on 27/01/14.
//  Copyright (c) 2014 com.dymextech. All rights reserved.
//

#import "GTProjectServices.h"

@implementation GTProjectServices

#pragma - Initialization

+(GTProjectServices *)instance
{
	static GTProjectServices * projectServices = nil;
	if (!projectServices)
	{
		projectServices = [[super allocWithZone:nil] init];
	}
	return projectServices;
}

+(id)allocWithZone:(struct _NSZone *)zone
{
	return [self instance];
}


#pragma - Operations

-(void)findProjectsWithClientId:(NSString *)clientId block:(FindProjectsBlock)block
{
	[PFCloud callFunctionInBackground:@"findProjectsWithClientId"
					   withParameters:@{@"clientId":clientId}
								block:^(NSArray * projects, NSError * error)
	 {
		 if (!error)
		 {
			 block(projects, nil);
		 }
		 else
		 {
			 block(nil, error);
			 NSLog(@"Error: %@ %@", error, [error userInfo]);
		 }
	 }];
}

@end
