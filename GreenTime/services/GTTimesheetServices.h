//
//  GTTimesheetServices.h
//  GreenTime
//
//  Created by Eduardo Dias on 28/01/14.
//  Copyright (c) 2014 com.dymextech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Timesheet.h"

typedef void (^SaveTimesheetBlock) (Timesheet * timesheet, NSError * error);
typedef void (^DeleteTimesheetBlock) (bool success, NSError * error);
typedef void (^ClientsWithTimesheetBlock) (NSArray * clientsTimesheet, NSError * error);
typedef void (^ProjectsWithTimesheetBlock) (NSArray * projectsTimesheet, NSError * error);

@interface GTTimesheetServices : NSObject

+(GTTimesheetServices *)instance;
-(void)saveTimesheet:(NSDictionary *)parameters block:(SaveTimesheetBlock)block;
-(void)deleteTimesheet:(NSString *)timesheetId block:(DeleteTimesheetBlock)block;
-(void)clientsTimesheetInDateRange:(NSDate *)startDate
						   endDate:(NSDate *)endDate
							 block:(ClientsWithTimesheetBlock)block;
-(void)projectsTimesheetInDateRange:(NSString *)clientId
						  startDate:(NSDate *)startDate
							endDate:(NSDate *)endDate
							  block:(ProjectsWithTimesheetBlock)block;
@end
