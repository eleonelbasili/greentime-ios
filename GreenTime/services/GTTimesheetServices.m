//
//  GTTimesheetServices.m
//  GreenTime
//
//  Created by Eduardo Dias on 28/01/14.
//  Copyright (c) 2014 com.dymextech. All rights reserved.
//

#import "GTTimesheetServices.h"

@implementation GTTimesheetServices

#pragma - Initialization

+(GTTimesheetServices *)instance
{
	static GTTimesheetServices * timesheetServices = nil;
	if (!timesheetServices)
	{
		timesheetServices = [[super allocWithZone:nil] init];
	}
	return timesheetServices;
}

+(id)allocWithZone:(struct _NSZone *)zone
{
	return [self instance];
}

-(void)saveTimesheet:(NSDictionary *)parameters block:(SaveTimesheetBlock)block
{
	[PFCloud callFunctionInBackground:@"saveTimesheet"
					   withParameters:parameters
								block:^(Timesheet * timesheet, NSError * error)
	 {
		 block(timesheet, error);
		 if (error)
		 {
			 NSLog(@"Error: %@ %@", error, [error userInfo]);
		 }
	 }];
}

-(void)deleteTimesheet:(NSString *)timesheetId block:(DeleteTimesheetBlock)block
{
	[PFCloud callFunctionInBackground:@"deleteTimesheet"
					   withParameters:@{@"timesheetId":timesheetId}
								block:^(Timesheet * timesheet, NSError * error)
	 {
		 block(timesheet, error);
		 if (error)
		 {
			 NSLog(@"Error: %@ %@", error, [error userInfo]);
		 }
	 }];
}

-(void)clientsTimesheetInDateRange:(NSDate *)startDate
							   endDate:(NSDate *)endDate
								 block:(ClientsWithTimesheetBlock)block
{
	[PFCloud callFunctionInBackground:@"clientsTimesheetInDateRange"
					   withParameters:@{@"startTime":startDate, @"endTime":endDate}
								block:^(NSArray * clientsTimesheet, NSError * error)
	 {
		 block(clientsTimesheet, error);
		 if (error)
		 {
			 NSLog(@"Error: %@ %@", error, [error userInfo]);
		 }
	 }];
}

-(void)projectsTimesheetInDateRange:(NSString *)clientId
						  startDate:(NSDate *)startDate
							endDate:(NSDate *)endDate
							  block:(ProjectsWithTimesheetBlock)block
{
	[PFCloud callFunctionInBackground:@"projectsTimesheetInDateRange"
					   withParameters:@{@"clientId":clientId, @"startTime":startDate, @"endTime":endDate}
								block:^(NSArray * clientsTimesheet, NSError * error)
	 {
		 block(clientsTimesheet, error);
		 if (error)
		 {
			 NSLog(@"Error: %@ %@", error, [error userInfo]);
		 }
	 }];
}

@end
