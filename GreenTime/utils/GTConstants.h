//
//  GTConstants.h
//  GreenTime
//
//  Created by Eduardo Dias on 4/09/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import <Foundation/Foundation.h>

#define IS_IPHONE_5 ([[UIScreen mainScreen] bounds].size.height == 568)

// Parse
extern NSString * const kGTParseAppId;
extern NSString * const kGTParseClientKey;

// Twitter
extern NSString * const kGTTwitterConsumerKey;
extern NSString * const kGTTwitterConsumerSecret;

// Fonts
extern NSString * const kGTFont;
extern NSString * const kGTFontBold;
extern CGFloat const kGTFontSizeSmall;
extern CGFloat const kGTFontSizeMedium;
extern CGFloat const kGTFontSizeLarge;

// General
extern CGFloat const kGTCornerRadius;
extern CGFloat const kGTDistanceToMatchLocation;
extern CGFloat const kGTTimeInterval;

// Settings
extern NSString * const kGTTimesheetReportDisplaySeconds;
extern NSString * const kGTPreferredDateFormat;
extern NSString * const kGTCompactDateFormat;
extern NSString * const kGTFullDateFormat;

@interface GTConstants : NSObject
@end
