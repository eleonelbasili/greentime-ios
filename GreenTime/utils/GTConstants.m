//
//  GTConstants.m
//  GreenTime
//
//  Created by Eduardo Dias on 4/09/13.
//  Copyright (c) 2013 com.dymextech. All rights reserved.
//

#import "GTConstants.h"

@implementation GTConstants : NSObject 

// Parse Production
//NSString * const kGTParseAppId = @"qorVlERqLe58Mb5vb5oQcX3fYHrzu5Vp5NUmfGPu";
//NSString * const kGTParseClientKey = @"PoPpGhdVhpR3KDSnM6Zkv4IyfXaDw56k5DF5xh7I";

// Parse Dev
NSString * const kGTParseAppId = @"5bYQL4ge5WMNfoo8Ut5Y7mHpKpjhJcfDZEJkBSRP";
NSString * const kGTParseClientKey = @"sDRbPkdygOCBXGFYSlrscBPuPAJl4osEuwkl2vtz";

// Twitter
NSString * const kGTTwitterConsumerKey = @"wIVgL8rCplWcLMi5kXGOqg";
NSString * const kGTTwitterConsumerSecret = @"G9NKFdtipHxnRpAXS80K37FRqg7Uua2RnUYvGXHieY";

// Fonts
NSString * const kGTFont = @"HelveticaNeue";
NSString * const kGTFontBold = @"HelveticaNeue-bold";
const CGFloat kGTFontSizeSmall = 13.0;
const CGFloat kGTFontSizeMedium = 14.0;
const CGFloat kGTFontSizeLarge = 20.0;

// General
const CGFloat kGTCornerRadius = 3.0f;
const CGFloat kGTDistanceToMatchLocation = 500.0;
const CGFloat kGTTimeInterval = 60.0;

// Settings
NSString * const kGTTimesheetReportDisplaySeconds = @"timesheetReportDisplaySeconds ";
NSString * const kGTPreferredDateFormat = @"preferredDateFormat";
NSString * const kGTCompactDateFormat = @"compactDateFormat";
NSString * const kGTFullDateFormat = @"fullDateFormat";

@end
