//
//  GTClientServicesTests.m
//  GreenTime
//
//  Created by Eduardo Dias on 25/02/14.
//  Copyright (c) 2014 com.dymextech. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "OCMock.h"
#import "GTClientServicesTest.h"
#import "XCTestCase+AsyncTesting.h"

@interface GTClientServicesTests : XCTestCase
{
	id serviceMock;
}
@end

@implementation GTClientServicesTests

- (void)setUp
{
    [super setUp];
    serviceMock = [OCMockObject partialMockForObject:[GTClientServicesTest instance]];
}

- (void)tearDown
{
    [super tearDown];
	serviceMock = nil;
}

-(void)saveClientTestWithSuccess:(NSDictionary *)params block:(SaveClientBlock)block
{
	block([[Client alloc] init], nil);
}

-(void)testSaveClientWithSuccess
{
    id block = ^void(Client * client, NSError * error)
	{
		XCTAssertNotNil(client, @"Client should not be null");
		XCTAssertNil(error, @"Error should be null");
	};
	[[[serviceMock stub]andCall:@selector(saveClientTestWithSuccess:block:)
					   onObject:self] saveClient:@{} block:block];
	[serviceMock saveClient:@{} block:block];
}

-(void)saveClientTestWithError:(NSDictionary *)params block:(SaveClientBlock)block
{
	block(nil, [[NSError alloc] init]);
}

-(void)testSaveClientFailed
{
    id block = ^void(Client * client, NSError * error)
	{
		XCTAssertNil(client, @"Client should be null");
		XCTAssertNotNil(error, @"Error should not be null");
	};
	[[[serviceMock stub]andCall:@selector(saveClientTestWithError:block:)
					   onObject:self] saveClient:@{} block:block];
	[serviceMock saveClient:@{} block:block];
}

@end
